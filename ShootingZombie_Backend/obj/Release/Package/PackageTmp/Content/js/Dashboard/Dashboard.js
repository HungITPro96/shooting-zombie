﻿(function () {
    zombie.module.Dashboard = function () {

        var tblLog;
        var LogDataSource;
        var tblLogData;
        var totalRows;
        var iRowPerPage = 10;

        function animate() {
            setTimeout(function () {
                $('p.animated-me').addClass('animated bounceIn');
            }, 1100);
            setTimeout(function () {
                $('p.animated-me').removeClass('animated bounceIn');
            }, 3000);
        }

        function defineElement() {
            var stats = [
                    { value: 30, date: new Date("2017/12/20") },
                    { value: 50, date: new Date("2017/12/21") },
                    { value: 45, date: new Date("2017/12/22") },
                    { value: 40, date: new Date("2017/12/23") },
                    { value: 35, date: new Date("2017/12/24") },
                    { value: 40, date: new Date("2017/12/25") },
                    { value: 42, date: new Date("2017/12/26") },
                    { value: 40, date: new Date("2017/12/27") },
                    { value: 35, date: new Date("2017/12/28") },
                    { value: 43, date: new Date("2017/12/29") },
                    { value: 38, date: new Date("2017/12/30") },
                    { value: 30, date: new Date("2017/12/31") },
                    { value: 48, date: new Date("2018/01/01") },
                    { value: 50, date: new Date("2018/01/02") },
                    { value: 55, date: new Date("2018/01/03") },
                    { value: 35, date: new Date("2018/01/04") },
                    { value: 30, date: new Date("2018/01/05") }
            ];

            $("#chartPlayInMonth").kendoChart({
                title: {
                    text: "Player by Time"
                },
                dataSource: {
                    data: stats
                },
                series: [{
                    type: "line",
                    field: "value",
                    categoryField: "date",
                    color: "#7460ee"
                }],
                categoryAxis: {
                    baseUnit: "days"
                },
                tooltip: {
                    visible: true,
                    template: "#:value# player"
                },
            });

            LogDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/DesktopModule/Dashboard/Service.asmx/LogList",
                    },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            var curDate = new Date();
                            data.startDate = new Date(curDate.getFullYear(), curDate.getMonth()-1,1);
                            data.endDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), 23, 59, 59);
                            data.type = 5;
                            data.GetByAccount = 3;
                            return JSON.stringify(data);
                        }
                    }
                },
                schema: {
                    data: function (response) {
                        return tblLogData;
                    },
                    total: function (response) {
                        return totalRows;
                    }
                },
                serverSorting: true,
                sort: {},
                serverPaging: true,
                pageSize: iRowPerPage,
                requestEnd: function (e) {
                    if (typeof (e.response) != "undefined") {
                        tblLogData = JSON.parse(e.response.d);
                        if (tblLogData.length > 0)
                            totalRows = tblLogData[0]["TotalRows"];
                        else totalRows = 0;
                    }
                }
            });

            tblLog = $('#tblLog').kendoGrid({
                autoBind: true,
                dataSource: LogDataSource,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                selectable: false,
                sortable: true,
                rowTemplate: kendo.template($("#rowTemplate").html()),
                columns: [
                        { field: "LogId", title: "Id", width: "5%" },
                        { field: "Title", title: "Title", width: "30%" },
                        { field: "Message", title: "Message" },
                        { field: "DateCreated", title: "Date Created", width: "15%" },
                        { field: "Name", title: "Name", width: "10%" }
                ]
            }).data('kendoGrid');
        }

        function defineEventHandler() {

        }


        (function () {
            defineElement();
            defineEventHandler();
            animate();
        }).call(this);
    }
})();

curModule = new zombie.module.Dashboard();