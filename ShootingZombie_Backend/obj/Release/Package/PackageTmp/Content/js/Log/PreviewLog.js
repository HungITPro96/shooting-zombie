﻿(function () {
    zombie.module.Log.PreviewLog = function () {

        this.InitData = function (id) {
            $('#PreviewLog .log-title').removeClass().addClass('log-title');
            $.ajax({
                url: "/DesktopModule/Log/Service.asmx/LogGetById",
                data: JSON.stringify({ LogId: id }),
                success: function (msg) {
                    if (msg.d == '-2') {
                        notificationMaster.show('Access is denied', 'error');
                    } else if (msg.d == '-1') {
                        notificationMaster.show('Error! Try again!', 'error');
                    } else {
                        var iLog = JSON.parse(msg.d);
                        $('#PreviewLog .log-title').text(iLog.Title);
                        if(iLog.AccountId>0)
                            $('#PreviewLog .log-account').text(iLog.AccName);
                        else $('#PreviewLog .log-account').text(iLog.FullName);
                        $('#PreviewLog .log-time').text(kendo.toString((new Date(parseInt(iLog.DateCreated.substr(6)))), "dd/MM/yyyy HH:mm"));
                        $('#PreviewLog .log-content').text(iLog.Message);
                        if (iLog.Type == 5)
                            $('#PreviewLog .log-title').addClass('log-titleError');
                        else
                            $('#PreviewLog .log-title').addClass('log-titleDefault');
                    }
                }
            })
        }

        function defineElement() {

        }

        function defineEventHandler() {

        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);

    }
})();