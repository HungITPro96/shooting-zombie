﻿$(document).ready(function () {
    var validatorLogin = $('#frmLogin');
    validatorLogin.validate();
    var sessionID = abcxyz;
    $('#txtUserName').rules("add", {
        required: true,
        messages: {
            required: "* User name is required"
        }
    });

    $('#txtPassword').rules("add", {
        required: true,
        messages: {
            required: "* Password is required"
        }
    });

    $('#btnSubmit').click(function (e) {
        e.preventDefault();
        if (validatorLogin.valid()) {
            var $this = $(this);
            $(this).css('cursor', 'not-allowed');
            $this.button('loading');
            $('#frmLogin').trigger('submit');
        }
    });

    $('#frmLogin').submit(function (e) {
        e.preventDefault();
        $('#info').closest('div.input-group').hide();
        var encrypted = CryptoJS.AES.encrypt($('#txtUserName').val(), sessionID);
        var decrypted = CryptoJS.AES.encrypt($('#txtPassword').val(), sessionID);
        $('#hidUsr').val(encrypted);
        $('#hidPass').val(decrypted);

        $.ajax({
            url: "/DesktopModule/Login/Service.asmx/Login",
            data: JSON.stringify({ pUser: $('#hidUsr').val(), pPass: $('#hidPass').val() }),
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {

                setTimeout(function (e) {
                    $('#btnSubmit').button().css('cursor', 'pointer');
                    $('#btnSubmit').button('reset');
                }, 1000);

                if (msg.d == "-3") {
                    $('#btnSubmit').button().css('cursor', 'pointer');
                    $('#btnSubmit').button('reset');
                    $('#info').closest('div.input-group').show();
                    $('#info').text('Username or password is invalid. Try again!');
                } else if (msg.d == "-2") {

                } else if (msg.d == "-1") {

                } else {
                    window.location.href = '/';
                }
            },
            error: function (msg) {
                setTimeout(function (e) {
                    $('#btnSubmit').button().css('cursor', 'pointer');
                    $('#btnSubmit').button('reset');
                }, 1000);
            }
        });
    })
});