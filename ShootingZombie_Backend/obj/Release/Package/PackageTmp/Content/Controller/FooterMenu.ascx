﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterMenu.ascx.cs" Inherits="ShootingZombie_Backend.Content.Controller.FooterMenu" %>

<nav class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" style="font-size:17px;">&copy; Coryright by 2017 by Shooting Zombie</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a style="font-size:17px;">Shooting Zombie</a></li>
        </ul>
    </div>
</nav>
