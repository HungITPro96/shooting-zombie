﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenu.ascx.cs" Inherits="ShootingZombie_Backend.Content.Controller.TopMenu" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topMenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="/">Shooting Zombie</a>
        </div>
        <div class="collapse navbar-collapse" id="topMenu">
            <ul class="nav navbar-nav">
                <% if (CheckPermission("Full_Permission")) { %>
                    <li rel="Dashboard"><a href="/?m=Dashboard"><i class="fa fa-area-chart" aria-hidden="true"></i>&nbsp;Dashboard</a></li>
                <%} %>
                <% if(CheckPermission("Full_Permission")){ %>
                    <li rel="Game"><a href="/?m=Game"><i class="fa fa-gamepad" aria-hidden="true"></i>&nbsp;Game</a></li>
                <%} %>
                <% if(CheckPermission("Full_Permission")){ %>
                    <li rel="News"><a href="/?m=News"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;News</a></li>
                <%} %>
                <% if(CheckPermission("Full_Permission")){ %>
                    <li rel="Item"><a href="/?m=Item"><i class="fa fa-rocket" aria-hidden="true"></i>&nbsp;Items Game</a></li>
                <%} %>
                <% if(CheckPermission("Full_Permission")){ %>
                    <li rel="Media"><a href="/?m=Media"><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;Media Game</a></li>
                <%} %>
                <% if (CheckPermission("Full_Permission")){ %>
                    <li rel="Log">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;System
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <% if(CheckPermission("Full_Permission")) {%>
                                <li rel="Log"><a href="/?m=Log"><i class="fa fa-tags" aria-hidden="true"></i>&nbsp;Log manager</a></li>
                            <%} %>
                        </ul>

                    </li>
                <%} %>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <% if(sFullName!="") { %>
                    <li>
                        <a href="#"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Welcome <%=sFullName %></a>
                    </li>
                    <li><a href="/?m=Logout"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a></li>
                <% }%>
            </ul>
        </div>
    </div>
</nav> 
<script type="text/javascript">
    $(document).ready(function () {
        var sModule = "<%= sModule%>";
        $('ul li').each(function () {
            var rel = $(this).attr('rel');
            if (rel != undefined)
                if (rel.indexOf('-' + sModule + '-') >= 0 || rel.indexOf(sModule) >= 0) {
                    $(this).addClass('active');
                }
        });
    })
</script>