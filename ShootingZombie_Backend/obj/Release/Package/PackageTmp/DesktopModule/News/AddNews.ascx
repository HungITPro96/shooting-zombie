﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNews.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.News.AddNews" %>

<style>
    .form-horizontal .form-group{
        margin-right:0;
        margin-left:0;
    }
    .form-horizontal .control-label{
        text-align:left;
        margin-bottom:5px;
    }
    .mt-20{
        margin-top:10px;
    }
    
</style>

<form role="form" class="form-horizontal" id="frmAddNews">
     <div class="form-group">
        <span id="parentName"></span>
        <input type="hidden" id="NewsGroupId" name="NewsGroupId" />
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="N_txtTitle">Title:</label>
        <div class="col-sm-11">
            <input type="text" class="form-control" id="N_txtTitle" placeholder="Title" name="N_txtTitle" required validationMessage="Title is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="N_txtAlias">Alias:</label>
        <div class="col-sm-11">
            <input type="text" class="form-control" id="N_txtAlias" placeholder="Alias" name="N_txtAlias" readonly="true" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="N_txtHeadline">Headline:</label>
        <div class="col-sm-11">
            <input type="text" class="form-control" id="N_txtHeadline" placeholder="Headline" name="N_txtHeadline" required validationMessage="Headline is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="N_txtStory">Story:</label>
        <div class="col-sm-11">
            <textarea class="form-control" rows="3" id="N_txtStory" placeholder="Story" name="N_txtStory" required validationMessage="Story is requied"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-5" style="padding-left:0;">
            <label class="control-label col-sm-2" for="N_txtStatus">Status:</label>
            <div class="col-sm-10">
                <input id="N_txtStatus" name="N_txtStatus" />
            </div>
        </div>
        <div class="col-md-5">
            <label class="control-label col-sm-2" for="N_txtType">Type:</label>
            <div class="col-sm-10">
                <input id="N_txtType" name="N_txtType" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4">
            <div class="row">
                <img src="<%=Page.ResolveClientUrl("/Content/images/default.png") %>" id="N_imgThumbImage" class="img-reponsive" width="150px" />
                <input id="N_txtThumbImage" name="N_txtThumbImage" type="hidden" validationmessage="Image is required" />
            </div>
            <div class="row mt-20">
                <button class="btn btn-primary add-pic" id="btnAddThumbImage" ref="N_imgThumbImage" rel="N_txtThumbImage" type="button">Add Image</button>
                <button class="btn btn-link del-pic" id="btnDeleteThumbImage" ref="N_imgThumbImage" rel="N_txtThumbImage" type="button">Delete</button>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <img src="<%=Page.ResolveClientUrl("/Content/images/default.png") %>" class="img-reponsive" id="N_imgThumbImageLarge" width="220px" />
                <input id="N_txtThumbImageLarge" name="N_txtThumbImageLarge" type="hidden" validationmessage="Image is required" />
            </div>
            <div class="row mt-20">
                <button class="btn btn-primary add-pic" id="btnAddThumbImageLarge" ref="N_imgThumbImageLarge" rel="N_txtThumbImageLarge" type="button">Add Image Large</button>
                <button class="btn btn-link del-pic" id="btnDeleteThumbImageLarge" ref="N_imgThumbImageLarge" rel="N_txtThumbImageLarge" type="button">Delete</button>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" id="N_btnAdd">Add</button>
            <button type="button" class="btn btn-link" id="N_btnCancel">Cancel</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/News/AddNews.js") %>"></script>