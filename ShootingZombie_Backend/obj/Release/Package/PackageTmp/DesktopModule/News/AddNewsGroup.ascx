﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewsGroup.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.News.AddNewsGroup" %>
<style>
    .form-horizontal .form-group{
        margin-right:0;
        margin-left:0;
    }
    .form-horizontal .control-label{
        text-align:left;
        margin-bottom:5px;
    }
</style>


<form role="form" class="form-horizontal" id="frmAddNewsGroup">
    <div class="form-group">
        <input type="hidden" id="parentId" name="parentId" />
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="NG_txtName">Name group:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="NG_txtName" placeholder="Name group" name="NG_txtName" required validationMessage="Name is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="NG_txtAlias">Alias:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="NG_txtAlias" placeholder="Alias" name="NG_txtAlias" readonly="true" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="NG_txtStatus">Status:</label>
        <div class="col-sm-10">
            <input id="NG_txtStatus" name="NG_txtStatus" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" id="NG_btnAdd">Add</button>
            <button type="button" class="btn btn-link" id="NG_btnCancel">Cancel</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/News/AddNewsGroup.js") %>"></script>