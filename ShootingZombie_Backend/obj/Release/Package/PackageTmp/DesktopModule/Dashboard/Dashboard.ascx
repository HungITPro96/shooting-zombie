﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.Dashboard.Dashboard" %>
<title>Dashboard - Shooting Zombie</title>
<style>
    .playing{
        background-color:#33bf7a;
        border-color:#33bf7a;
    }
    .online{
        background-color:#0a89c1;
    }
    .in-month{
        background-color:#7460ee;
    }
    .error-log{
        background-color: #f95454;
    }
    .section-heading{
        font-size:38px;
        margin:0;
    }
    .dashboard{
        padding-top:15px;
    }
    .panel-default {
        color:#fff;
    }
    .panel-default i{
        color:#fff;
    }
    .panel-default .panel-heading .panel-title h3{
        margin-top:0;
        margin-bottom:0;
        font-size:18px;
        color:#22768f;
        border-bottom: 1px solid #b6b6b6;
        padding-bottom:5px;
    }
    .panel-default .panel-heading{
        background-color:#fff;
        border:none;
    }
    /*.log-error, .log-error a.title{
        background-color:#FCE4EC;
        color:#ef5350;
    }*/
</style>
<div class="container dashboard">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default playing">
                <div class="panel-body ">
                    <div class="col-md-4">
                        <i class="fa fa-play fa-5x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-8 text-right">
                        <p class="section-heading animated-me">75</p>
                        <p class="section-content">Now Playing</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default online">
                <div class="panel-body">
                    <div class="col-md-4">
                        <i class="fa fa-user fa-5x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-8 text-right">
                        <p class="section-heading animated-me">101</p>
                        <p class="section-content">Online</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default in-month">
                <div class="panel-body">
                    <div class="col-md-4">
                        <i class="fa fa-gamepad fa-5x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-8 text-right">
                        <p class="section-heading animated-me">1020</p>
                        <p class="section-content">Play in Month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default error-log">
                <div class="panel-body">
                    <div class="col-md-4">
                        <i class="fa fa-exclamation-circle fa-5x" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-8 text-right">
                        <p class="section-heading animated-me">3</p>
                        <p class="section-content">Total Error</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h3>Statistic Player</h3>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="chartPlayInMonth"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h3>List Log Error</h3>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="tblLog"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/x-kendo-template" id="rowTemplate">
    # var addClass = ""; 
        if(Type==5){ addClass="log-error";}
    #
    <tr>
        <td class="#:addClass#">
            #: LogId#
        </td>
        <td class="#:addClass#">
            # var m = "";
                if(Title.length>50)
                    m = Title.substring(0,50) + "...";
                else m = Title;
            #
            #: m#
        </td>
        <td class="#:addClass#">
            # var m = "";
                if(Message.length>80)
                    m = Message.substring(0,80) + "...";
                else m = Message;
            #
            #: m#
        </td>
        <td class="#:addClass#">
            #: kendo.toString((new Date(parseInt(DateCreated.substr(6)))),"dd/MM/yyyy HH:mm") #
        </td>
        <td class="#:addClass#">
            # var name = "Unknown";
                if(Name != "null")
                    name = Name;
            #
            #: name#
        </td>
    </tr>
</script>
<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/Dashboard/Dashboard.js") %>"></script>
