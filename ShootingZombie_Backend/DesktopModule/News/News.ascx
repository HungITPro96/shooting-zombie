﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.News.News" %>
<title>News - Shooting Zombie</title>
<style>
#treeViewNewsGroup .glyphicon{
    color: #a8a7a7;
    display:none;
}
.sideBar .glyphicon-ok-circle{
    color:#7ab23b !important;
}
.text-banned{
    color: #4d4c4c;
}
tr.k-state-selected a.news-title{
    color: #fff;
}
</style>
<div class="row">
    <span id="notification"></span>
    <div id="dialog"></div>
    <div class="col-md-2 sideBar">
        <button class="btn btn-primary" id="btnAddNewsGroup"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Newsgroup</button>
        <button class="btn btn-primary" id="btnEditNewsGroup"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit</button>
        <hr />
        <h4 id="rootNode" style="cursor:pointer;"><span class="label label-primary">Root category</span></h4>
        <div id="treeViewNewsGroup"></div>
    </div>
    <div class="col-md-10 col-md-offset-2">
        <h3 class="newsGroupName" rel="">
            <label></label>
        </h3>
        <div id="tblMain"></div>
        <div id="addNews"></div>
        <div id="addNewsGroup"></div>
        <div id="editNews"></div>
        <div id="editNewsGroup"></div>
    </div>
</div>
<script type="text/x-kendo-template" id="treeview-template">
    # if(item.Status == 1){ #
        <span class="text">#: item.Name #</span>
    # }else{ #
        <span class="text text-banned">#: item.Name #</span>
    # } #
    # if (item.NewsGroupId != 0) { #
        # if(item.Status == 1){ #
            <a class="glyphicon glyphicon-ok-circle" ref="#:item.NewsGroupId#" href="\#" title="Activate"></a>
        # }else if(item.Status){ #
            <a class="glyphicon glyphicon-ban-circle" ref="#:item.NewsGroupId#" href="\#" title="Hide"></a>
        # } #
        <%if (CheckPermission("Full_Permission"))
    { %>
            <a class="glyphicon glyphicon-edit" ref="#:item.NewsGroupId#" href="\#" title="Edit"></a>
        <%} %>
        <%if (CheckPermission("Full_Permission"))
    { %>
            <a class="glyphicon glyphicon-trash" ref="#:item.NewsGroupId#" href="\#" title="Delete"></a>
        <%} %>
    # } #
</script>
<script type="text/x-kendo-template" id="rowTemplate">
    <tr>
        <td align="center">
            <input type="checkbox" id="cbk#:NewsId#" name="cbkNews" ref="#:NewsId#" status="#:Status#" class="Check-item"/>
        </td>
        <td>#: NewsId#</td>
        <td><a href="\#" ref="#:NewsId#" class="news-title">#: Title#</a></td>
        <td># var m = "";
                if(Story.length>80)
                    m = Story.substring(0,80) + "...";
                else m = Story;
            #
            #: m#
        </td>
        <td># if(Status == 1) {#
                Active
            # }else if (Status == 2){#
                Hidden
            #} else {#
                Other
            #}#
        </td>
        <td># if(Type == 1) {#
                Normal News
            # }else if (Type == 2){#
                Banner News
            #} else {#
                Other
            #}#
        </td>
    <%if (CheckPermission("Full_Permission"))
    { %>
            <td align="center"><a class="glyphicon glyphicon-trash" ref="#:NewsId#" href="\#" title="Delete"></a></td>
        <%} %>
    </tr>
</script>
<script type="text/x-kendo-template" id="toolbarTemplate">
    <div>
        <button type="button" class="btn btn-primary" id="btnAddNews"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add News</button>
        <button type="button" class="btn btn-primary" id="btnDeleteNews" style="display:none;"><i class="glyphicon glyphicon-remove"></i>&nbsp;Delete News selected</button>
        <input id="filterStatus" class="pull-right"/>
    </div>
</script>

<script src="<%=Page.ResolveClientUrl("~/Content/ckeditor/ckeditor.js")%>"></script>
<script src="<%=Page.ResolveClientUrl("~/Content/ckfinder/ckfinder.js")%>"></script>
<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/News/News.js") %>"></script>