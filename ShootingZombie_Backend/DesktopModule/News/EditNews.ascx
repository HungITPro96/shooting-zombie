﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditNews.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.News.EditNews" %>
<style>
    .form-horizontal .form-group{
        margin-right:0;
        margin-left:0;
    }
    .form-horizontal .control-label{
        text-align:left;
        margin-bottom:5px;
    }
    
</style>

<form role="form" class="form-horizontal" id="frmEditNews">
     <div class="form-group">
        <span id="parentName"></span>
        <input type="hidden" id="NewsId" name="NewsId" />
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="EN_txtTitle">Title:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="EN_txtTitle" placeholder="Title" name="EN_txtTitle" required validationMessage="Title is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="EN_txtAlias">Alias:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="EN_txtAlias" placeholder="Alias" name="EN_txtAlias" readonly="true" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="EN_txtHeadline">Headline:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="EN_txtHeadline" placeholder="Headline" name="EN_txtHeadline" required validationMessage="Headline is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="EN_txtStory">Story:</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="3" id="EN_txtStory" placeholder="Story" name="EN_txtStory" required validationMessage="Story is requied" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-5" style="padding-left:0;">
            <label class="control-label col-sm-2" for="EN_txtStatus">Status:</label>
            <div class="col-sm-10">
                <input id="EN_txtStatus" name="EN_txtStatus" />
            </div>
        </div>
        <div class="col-md-5">
            <label class="control-label col-sm-2" for="EN_txtType">Type:</label>
            <div class="col-sm-10">
                <input id="EN_txtType" name="EN_txtType" />
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" id="EN_btnAdd">Save</button>
            <button type="button" class="btn btn-link" id="EN_btnCancel">Cancel</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/News/EditNews.js") %>"></script>