﻿using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShootingZombie_Backend.DesktopModule.News
{
    public partial class News : System.Web.UI.UserControl
    {
        private string[] lstPermission = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Dashboard");
            lstPermission = (string[])Session["z_ListPermission"];
        }
        public bool CheckPermission(string p)
        {
            return PermissionHelper.CheckPermission(lstPermission, p);
        }
    }
}