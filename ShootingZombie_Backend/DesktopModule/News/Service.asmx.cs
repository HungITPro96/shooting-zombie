﻿using ShootingZombie_Backend.Content.Data;
using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace ShootingZombie_Backend.DesktopModule.News
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ListTreeNewsGroup(int NewsGroupId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                try
                {
                    string strJSON = "";
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        List<sp_NewsGroups_ListTree_Result> lstTree = dtc.sp_NewsGroups_ListTree(NewsGroupId, false).ToList();
                        strJSON = js.Serialize(lstTree);
                        return strJSON;
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddNewsGroup(FormEntity[] formData)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    string name = UrlHelper.getRequestParam(formData.GetSingleValue("NG_txtName"), "");
                    string alias = UrlHelper.getRequestParam(formData.GetSingleValue("NG_txtAlias"), "");
                    int status = UrlHelper.getRequestParam(formData.GetSingleValue("NG_txtStatus"), 0);
                    int parentId = UrlHelper.getRequestParam(formData.GetSingleValue("parentId"), 0);
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        ObjectParameter oNewsGroupId = new ObjectParameter("NewsGroupId", 0);
                        int? iResult = dtc.sp_NewsGroups_Insert(name, alias, status, parentId, oNewsGroupId).FirstOrDefault();
                        if (iResult == null)
                            return "0";
                        LogHelper.LogInsert("Thêm NewsGroup", "Thêm NewsGroupId: " + oNewsGroupId.Value, sAccId);
                        return iResult.ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateNewsGroupStatus(int NewsGroupId, int Status)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_NewsGroups_UpdateStatus(NewsGroupId, Status).FirstOrDefault();
                        if (iResult == null)
                            return "0";
                        LogHelper.LogUpdate("Sửa trạng thái NewsGroup", "Sửa trạng thái NewsGroup: " + NewsGroupId + " sang Status: " + Status, sAccId);
                        return iResult.ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsList(int iNewsGroupId, string startDate, string endDate, int status, int page, int pageSize, int skip, int take, string keyword, List<KendoSort> sort)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    string sOrderBy = "";
                    foreach (KendoSort iSort in sort)
                    {
                        sOrderBy += iSort.field + " " + iSort.dir + ", ";
                    }
                    if (sOrderBy.Length > 0)
                        sOrderBy = sOrderBy.Remove(sOrderBy.Length - 2);

                    DateTime dtStart, dtEnd;

                    if (startDate != "")
                        dtStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                    else
                        dtStart = DateTime.Now.AddMonths(-1);

                    if (startDate != "")
                        dtEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                    else
                        dtEnd = DateTime.Now;

                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        List<sp_News_List_Result> lstNews = dtc.sp_News_List(skip, take, sOrderBy, keyword, iNewsGroupId, dtStart, dtEnd, status, 0).ToList();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        return js.Serialize(lstNews);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsGroupCheckDeleteAvailable(int NewsGroupId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_NewsGroups_CheckDeleteAvailable(NewsGroupId).FirstOrDefault();
                        if (iResult == null)
                            return "0";
                        return iResult.GetValueOrDefault().ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsGroupDelete(int NewsGroupId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_NewsGroups_Delete(NewsGroupId).FirstOrDefault();
                        if (iResult == null)
                            return "0";

                        return iResult.GetValueOrDefault().ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddNews(Content.Logic.News iNews)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        ObjectParameter oNewsId = new ObjectParameter("NewsId", 0);
                        int? iResult = dtc.sp_News_Insert(iNews.Headline, iNews.Story, iNews.Title, sAccId, iNews.NewsGroupId, iNews.Status, "", "", iNews.Alias, iNews.Type, oNewsId).FirstOrDefault();
                        if (iResult == null)
                        {
                            return "0";
                        }
                        return iResult.GetValueOrDefault().ToString();

                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsGetById(int NewsId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        sp_News_GetById_Result iNews = dtc.sp_News_GetById(NewsId).FirstOrDefault();
                        if (iNews == null) return "0";
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        return js.Serialize(iNews);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateNews(Content.Logic.News iNews)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_News_Update(iNews.NewsId, iNews.Headline, iNews.Story, iNews.Title, iNews.Status, "", "", iNews.Alias, iNews.Type).FirstOrDefault();
                        if (iResult == null) return "0";
                        return iResult.GetValueOrDefault().ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsDelete(string lstNewsId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_News_Delete(lstNewsId).FirstOrDefault();
                        if (iResult == null)
                            return "0";
                        return iResult.GetValueOrDefault().ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string NewsGroupGetById(int NewsGroupId)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        sp_NewsGroups_GetById_Result iNewsGroup = dtc.sp_NewsGroups_GetById(NewsGroupId).FirstOrDefault();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        return js.Serialize(iNewsGroup);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateNewsGroup(FormEntity[] formData)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    string name = UrlHelper.getRequestParam(formData.GetSingleValue("ENG_txtName"), "");
                    string alias = UrlHelper.getRequestParam(formData.GetSingleValue("ENG_txtAlias"), "");
                    int status = UrlHelper.getRequestParam(formData.GetSingleValue("ENG_txtStatus"), 0);
                    int newsGroupId = UrlHelper.getRequestParam(formData.GetSingleValue("newsGroupId"), 0);

                    using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.sp_NewsGroups_Update(newsGroupId, name,alias, status).FirstOrDefault();
                        if (iResult == null) return "0";
                        return iResult.GetValueOrDefault().ToString();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

    }
}
