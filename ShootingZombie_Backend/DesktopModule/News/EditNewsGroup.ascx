﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditNewsGroup.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.News.EditNewsGroup" %>
<style>
    .form-horizontal .form-group{
        margin-right:0;
        margin-left:0;
    }
    .form-horizontal .control-label{
        text-align:left;
        margin-bottom:5px;
    }
</style>
<form role="form" class="form-horizontal" id="frmEditNewsGroup">
    <div class="form-group">
        <span id="newsGroupName"></span>
        <input type="hidden" id="newsGroupId" name="newsGroupId" />
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="ENG_txtName">Name group:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ENG_txtName" placeholder="Name group" name="ENG_txtName" required validationMessage="Name is requied" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="ENG_txtAlias">Alias:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="ENG_txtAlias" placeholder="Alias" name="ENG_txtAlias" readonly="true" />
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="ENG_txtStatus">Status:</label>
        <div class="col-sm-10">
            <input id="ENG_txtStatus" name="ENG_txtStatus" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" id="ENG_btnEdit">Update</button>
            <button type="button" class="btn btn-link" id="ENG_btnCancel">Cancel</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/News/EditNewsGroup.js") %>"></script>