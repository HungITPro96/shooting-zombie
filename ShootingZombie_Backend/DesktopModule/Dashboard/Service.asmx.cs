﻿using ShootingZombie_Backend.Content.Data;
using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace ShootingZombie_Backend.DesktopModule.Dashboard
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string LogList(string startDate, string endDate, int type, int page, int pageSize, int skip, int take, int GetByAccount, List<KendoSort> sort)
        {
            int sAccId = UrlHelper.getRequestParam(Convert.ToString(Session["z_UserId"]), 0);
            string[] lstPermission = (string[])Session["z_ListPermission"];
            if (PermissionHelper.CheckPermission(lstPermission, "Full_Permission"))
            {
                try
                {
                    string sOrderBy = "";

                    foreach (KendoSort iSort in sort)
                    {
                        sOrderBy += "[" + iSort.field + "] " + iSort.dir + ", ";
                    }
                    if (sOrderBy.Length > 0)
                        sOrderBy = sOrderBy.Remove(sOrderBy.Length - 2);
                    DateTime start = DateTime.Parse(startDate);
                    DateTime end = DateTime.Parse(endDate);
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        List<sp_Logs_List_Result> lstLog = dtc.sp_Logs_List((short)type, -1,0, start, end, skip, take, 0, GetByAccount, sOrderBy).Where(p => p.Status == 1).ToList();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        return js.Serialize(lstLog);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.LogError(ex, sAccId, 0);
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }
    }
}
