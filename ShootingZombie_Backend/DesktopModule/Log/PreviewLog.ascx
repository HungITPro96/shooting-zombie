﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreviewLog.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.Log.PreviewLog" %>

<style>
    #PreviewLog{
        margin-left:0;
        margin-right:0;
        padding:0 10px;
    }
    #PreviewLog div.col-md-12{
        padding-left:0;
        padding-right:0;
    }
    .divTitle{
        padding:10px;
    }
    .log-title{
        font-weight:bold;
        
        font-size:larger;
    }
    .log-subTitle{
        display:inline;
        margin-right: 8px;
    }
    .log-subTitleTime{
        color:#FBC02D;
    }
    .log-subTitleAccount{
        color:#03A9F4;
    }
    .divSubTitle{
        margin-bottom:5px;
    }
    .log-titleError{
        border-bottom: 1px solid #FCE4EC;
        color:#ef5350;
    }
    .log-titleDefault{
        border-bottom: 1px solid #d6d6d6;
        color:#03A9F4;
    }
    .log-content{
        display:inline;
    }
</style>

<div class="row" id="PreviewLog">
    <div class="col-md-12 divTitle">
        <div class="log-title"></div>
    </div>
    <div class="col-md-12 divSubTitle">
        <div class="log-subTitle log-subTitleAccount"><i class="glyphicon glyphicon-user"></i>&nbsp;<span class="log-account"></span></div>
        <div class="log-subTitle log-subTitleTime"><i class="glyphicon glyphicon-time"></i>&nbsp;<span class="log-time"></span></div>
    </div>
    <div class="col-md-12">
        Message:&nbsp;
        <div class="log-content">

        </div>
    </div>
</div>
<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/Log/PreviewLog.js") %>"></script>