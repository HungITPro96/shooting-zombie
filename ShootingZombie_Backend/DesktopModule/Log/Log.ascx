﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Log.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.Log.Log" %>
<style>
    #Log_Content{
        margin:auto;
        max-width:1000px;
    }
    tr.k-state-selected a.title{
        color:#fff;
    }
    .log-error, .log-error a.title{
        background-color:#FCE4EC;
        color:#ef5350;
    }
    .type-error{
        background-color:#FCE4EC;
        border: 1px solid #FCE4EC;
        color:#ef5350;
    }
    .type-warning{
        background-color:#f8f7b7;
        border: 1px solid #f8f7b7;
    }
    .filter-log{
        margin-bottom:5px;
    }
    .filter-log label{
        display:block;
    }
</style>
<title>Log Manager - Shooting Zombie</title>
<div class="row">
    <div id="Log_Content">
        <span id="notification"></span>
        <h3>Log Manager</h3>
        <div class="row filter-log">
            <div class="col-md-3">
                <label for="startTime">Start Date</label>
                <input id="startTime" name="startTime"/>
            </div>
            <div class="col-md-3">
                <label for="endTime">End Date</label>
                <input id="endTime" name="endTime"/>
            </div>
            <div class="col-md-3">
                <label for="logType">Type Log</label>
                <input id="logType" name="logType"/>
            </div>
            <div class="col-md-3">
                <label for="logStatus">Status</label>
                <input id="logStatus" name="logStatus"/>
            </div>
        </div>
        <div class="log-table">
            <div id="tblLog"></div>
        </div>
    </div>
</div>
<div id="Preview"></div>
<script type="text/x-kendo-template" id="rowTemplate">
    # var addClass = ""; 
        if(Type==5){ addClass="log-error";}
    #
    <tr>
        <td class="#:addClass#">
            #: LogId#
        </td>
        <td class="#:addClass#">
            # var m = "";
                if(Title.length>50)
                    m = Title.substring(0,50) + "...";
                else m = Title;
            #
            <a href="\#" rel="#: LogId#" class="title">#: m#</a>
        </td>
        <td class="#:addClass#">
            # var m = "";
                if(Message.length>80)
                    m = Message.substring(0,80) + "...";
                else m = Message;
            #
            #: m#
        </td>
        <td class="#:addClass#">
            #: kendo.toString((new Date(parseInt(DateCreated.substr(6)))),"dd/MM/yyyy HH:mm") #
        </td>
        <td class="#:addClass#">
            # var name = "Unknown";
                if(Name != "null")
                    name = Name;
            #
            #: name#
        </td>
        <td class="#:addClass# text-center">
            # if(Status!=null){ #
                # if(Status == 1){#
                    <a href="\#" class="btn btn-warning mark-done" ref="#:LogId#" title="Make done">Done</a>
                #} else if(Status == 2){#
                    <i class="fa fa-check-circle" aria-hidden="true" style="color:\#28a745;" title="Done"></i>
                #}#
            #}#
        </td>
    </tr>
</script>
<script type="text/x-kendo-template" id="template_LogType">
    # var addClass = ""; 
        if(value==5){ addClass="type-error";}
        else if(value==4){ addClass="type-warning";}
    #
    <span class="btn btn-sm #=addClass#" style="width:150px;text-align:left;margin-left:0px;">
        #: text#
    </span>
</script>
<script type="text/x-kendo-template" id="template_LogStatus">
    # if(value==2){ #
        <span style="color:\#28a745;">
            <i class="fa fa-check-circle-o" aria-hidden="true"title="Done"></i>&nbsp;#:text#
        </span>
    #} else if(value==1) {#
        <span style="color:\#ef5350;">
            <i class="fa fa-times-circle-o" aria-hidden="true" title="Undone"></i>&nbsp;#:text#
        </span>
    #} else{#
        <span>
            #:text#
        </span>
    #}#
</script>
<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/Log/Log.js") %>"></script>