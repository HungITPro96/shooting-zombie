﻿using ShootingZombie_Backend.Content.Data;
using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace ShootingZombie_Backend.DesktopModule.Login
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Login(string pUser, string pPass)
        {
            int sUserId = UrlHelper.getRequestParam((string)Session["z_UserId"], 0);
            if (sUserId == 0)
            {
                try
                {
                    CryptoUtils crypto = new CryptoUtils();
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        string sUserName = crypto.OpenSSLDecrypt(pUser, Session.SessionID);
                        string sPassword = crypto.OpenSSLDecrypt(pPass, Session.SessionID);
                        string md5Password = crypto.GetMD5Hash(sPassword);
                        sp_Accounts_CheckLogin_Result iAccount = dtc.sp_Accounts_CheckLogin(sUserName, md5Password).FirstOrDefault();
                        if (iAccount != null)
                        {
                            Session["z_UserId"] = iAccount.AccId;
                            Session["z_UserName"] = iAccount.AccName;
                            Session["z_FullName"] = iAccount.FullName;
                            string[] lstPermission = dtc.sp_Permisisons_GetListPermission(iAccount.AccId).ToArray<string>();
                            Session.Add("z_ListPermission", lstPermission);
                            LogHelper.LogSystem("Đăng nhập hệ thống", "AccountID: " + iAccount.AccId + ", AccountName: " + iAccount.AccName, iAccount.AccId, 0);
                            return "1";
                        }
                        else
                        {
                            return "-3";
                        }
                    }
                }catch(Exception ex)
                {
                    LogHelper.LogError(ex, sUserId, 0);
                    return "-1";
                }
            }else
            {
                return "-2";
            }
            

        }
    }
}
