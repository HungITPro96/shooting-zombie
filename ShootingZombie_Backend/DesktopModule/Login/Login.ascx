﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ShootingZombie_Backend.DesktopModule.Login.Login" %>
<title>Login - Shooting Zombie</title>
<style>
    body {
        background-color: #eee;
    }

    .container{
        margin-top:30px;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 30px auto;
        background-color:#fff;
        box-shadow:rgba(0, 0, 0, 0.1) 0px 6px 6px 6px;
        border-radius:10px;
    }

    .form-signin-heading {
        margin-bottom: 25px;
        font-size: 2rem;
        text-align:center;
    }
    .input-group{ 
  position:relative; 
  margin-bottom:12px; 
}
input.text{
  font-size:18px;
  padding:10px 10px 10px 5px;
  display:block;
  width:300px;
  border:none;
  border-bottom:1px solid #00BFFF;
}
input:focus{ outline:none; }
/* LABEL ======================================= */
label.forText {
  color:#999; 
  font-size:18px;
  font-weight:normal;
  position:absolute;
  pointer-events:none;
  left:5px;
  top:10px;
  transition:0.2s ease all; 
}

/* active state */
input.text:focus ~ label.forText, input.text:valid ~ label.forText     {
  top:-20px;
  font-size:14px;
  color:#00bfff;
}

/* BOTTOM BARS ================================= */
.bar  { position:relative; display:block; width:300px; }
.bar:before, .bar:after   {
  content:'';
  height:2px; 
  width:0;
  bottom:.5px; 
  position:absolute;
  background:#00BFFF; 
  transition:0.2s ease all; 
}
.bar:before {
  left:50%;
}
.bar:after {
  right:50%; 
}

/* active state */
input.text:focus ~ .bar:before, input.text:focus ~ .bar:after {
  width:50%;
}
.error{
    color: #ef4136;
}
#btnSubmit{
    color: #fff;
    background-color: #0062cc;
    border-color: #005cbf;
    box-shadow: 0 0 0 0.2rem rgba(0,123,255,.5);
    width:100%;
    display:block;
}
#btnSubmit:hover{
    color: #fff;
    background-color: #0069d9;
    border-color: #0062cc;
}
.glyphicon.spinning {
            animation: spin 1s infinite linear;
            -webkit-animation: spin2 1s infinite linear;
        }
 
        @keyframes spin {
            from { transform: scale(1) rotate(0deg);}
            to { transform: scale(1) rotate(360deg);}
        }
 
        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg);}
            to { -webkit-transform: rotate(360deg);}
        }
</style>
<div class="container">
    <form role="form" method="post" class="form-signin" id="frmLogin">
        <h2 class="form-signin-heading">Sign in</h2>
        <div class="input-group">
            <input type="text" class="text" required="required" autocomplete="true" id="txtUserName" name="txtUserName" />
            <span class="highlight"></span>
            <span class="bar"></span>
            <label class="forText">User Name</label>
            <div style="height: 25px;">
                <label id="txtUserName-error" class="error" for="txtUserName"></label>
            </div>
        </div>

        <div class="input-group">
            <input type="password" class="text" required="required" autocomplete="off" id="txtPassword" name="txtPassword"/>
            <span class="highlight"></span>
            <span class="bar"></span>
            <label class="forText">Password</label>
            <div style="height: 25px;">
                <label id="txtPassword-error" class="error" for="txtPassword"></label>
            </div>
        </div>
        <%--<div class="input-group">
            <input type="checkbox" id="chbRememberMe" />
            <label for="chbRememberMe">Remember me</label>
        </div>--%>
        <div class="input-group" style="display:none;">
            <span class="error" id="info"></span>
        </div>
        <input type="hidden" name="hidUsr" id="hidUsr" />
        <input type="hidden" name="hidPass" id="hidPass" />
        <button type="submit" class="btn btn-lg btn-primary" id="btnSubmit"  data-loading-text="<i class='glyphicon glyphicon-refresh spinning'></i>&nbsp;Signing in...">Sign in</button>
    </form>
</div>
<script type="text/javascript" src="/Content/js/jquery.validate.min.js"></script>
<script src="/Content/js/aes.js" type="text/javascript"></script>
<script type="text/javascript">
    var abcxyz = "<%=Session.SessionID%>";
</script>
<script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/Login/Login.js") %>"></script>
