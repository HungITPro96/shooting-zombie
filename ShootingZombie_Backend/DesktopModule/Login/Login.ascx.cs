﻿using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShootingZombie_Backend.DesktopModule.Login
{
    public partial class Login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Login");
            string sUserName = (string)Session["z_UserName"];
            if (!string.IsNullOrEmpty(sUserName))
            {
                Response.Redirect("/?m=Dashboard");
                return;
            }
        }
    }
}