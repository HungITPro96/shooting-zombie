﻿using ShootingZombie_Backend.Content.Logic;
using System;
using System.Web.UI;

namespace ShootingZombie_Backend
{
    public partial class Module : System.Web.UI.Page
    {
        protected string sPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string sPara = "", sChild = "";
            sPara = UrlHelper.getRequestParam(Request.Params["m"], "Dashboard");
            if (sPara.IndexOf("_") > 0)
            {
                sChild = sPara.Split('_')[1];
                sPara = sPara.Split('_')[0];
            }

            string sUserName = (string)Session["z_UserName"];
            
            if (sPara != "" && sPara != "Login")
            {
                if (string.IsNullOrEmpty(sUserName))
                {
                    Response.Redirect("/?m=Login");
                    return;
                }
            }
            if(sPara!="")
            {
                if (sChild != "")
                    sPath = "/DesktopModule/" + sPara + "/" + sChild + ".ascx";
                else
                    sPath = "/DesktopModule/" + sPara + "/" + sPara + ".ascx";

                UserControl ucrl = Page.LoadControl(sPath) as UserControl;
                if (ucrl != null)
                {
                    this.Controls.Add(ucrl);
                }
                else
                    return;

            }
        }
    }
}