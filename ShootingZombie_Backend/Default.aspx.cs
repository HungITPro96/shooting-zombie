﻿using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShootingZombie_Backend
{
    public partial class _Default : Page
    {
        protected string sLink = "";
        protected string strSecTckt = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            strSecTckt = Guid.NewGuid().ToString();
            string Key = "SecurityModule:" + Session.SessionID;
            Session[Key] = strSecTckt;
            string sPara = UrlHelper.getRequestParam(Request.Params["m"], "");

            if(sPara == "Logout")
            {
                Session.Clear();
                Response.Redirect("/");
                return;
            }
            else if(sPara == "")
            {
                string sUserName = UrlHelper.getRequestParam((string)Session["z_UserName"],"");
                if(sUserName != "")
                {
                    sLink = "Dashboard";
                }else
                {
                    sLink = "Login";
                }
            }
            else
            {
                sLink = sPara;
            }
        }
    }
}