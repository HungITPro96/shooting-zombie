﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Content/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ShootingZombie_Backend._Default" %>

<asp:Content ID="Header" ContentPlaceHolderID="MasterPage_Head" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="MasterPage_Top" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="preloader" class="modal-backdrop fade in">
        <div id="load"></div>
    </div>

    <div class="container-fluid" style="margin-top:51px;margin-bottom:51px;">
        <div id="siteContent"></div>
    </div>
    
    <script type="text/javascript">
        function getModule(link, parent) {
            $("#preloader").fadeIn(300);
            $.ajax({
                type: "GET",
                dataType: "html",
                url: "Module.aspx?m=" + link,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Zombie_HeaderTicket", "<%=strSecTckt %>");
                },
                success: function (e) {
                    setTimeout(function(){
                        $("#preloader").fadeOut();
                    },500); 
                    url = "?m=" + link;
                    var pageTitle = "Shooting Zombie";
                    if ($(e).filter('title').length > 0)
                        pageTitle = $(e).filter('title').text();

                    document.title = pageTitle;
                    window.history.pushState({ "html": $(document).html(), "pageTitle": pageTitle }, "", url);
                    $(parent).html(e);
                }
            });
        }
        (function () {
                zombie = function () {
                    var sLink = "<%=sLink %>";

                    getModule(sLink, "#siteContent");
                    $.ajaxSetup({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Zombie_HeaderTicket", "<%=strSecTckt %>");
                        }
                    });

                    zombie.module = zombie.module || {};
                }
        }).call(this);
        
        $(document).ready(function () {
            new zombie();
        })
    </script>
</asp:Content>
