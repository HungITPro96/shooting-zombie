﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShootingZombie_Backend.Content.Logic
{
    public class PermissionHelper
    {
        public static bool CheckPermission(string[] lstPermission, string iPermission)
        {
            if (lstPermission == null) return false;
            foreach (string permission in lstPermission)
            {
                if (permission == iPermission)
                    return true;
            }
            return false;
        }
    }
}