﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShootingZombie_Backend.Content.Logic
{
    public class FormEntity
    {
        public string name { get; set; }
        public string value { get; set; }
    }
    public static class FormHelper
    {
        public static string[] GetMultipleValue(this FormEntity[] formVars, string name)
        {
            //List<string> sValue = new List<string>();
            //foreach (FormEntity iValue in formVars)
            //{
            //    if()
            //}
            return new string[2];
        }
        public static string GetSingleValue(this FormEntity[] formVars, string name)
        {
            string sValue = "";
            foreach (FormEntity iValue in formVars)
            {
                if (iValue.name.Equals(name))
                    sValue = iValue.value;
            }
            return sValue;
        }
    }
}