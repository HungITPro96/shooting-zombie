﻿using ShootingZombie_Backend.Content.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShootingZombie_Backend.Content.Logic
{
    public class LogHelper
    {
        //1: System, 2: Insert, 3: Update, 4: Delete, 5: Error
        private static void Log(string sTitle, string sMessage, int Type, int AccountId, int UserId)
        {
            using(ShootingZombieEntities dtc = new ShootingZombieEntities())
            {
                decimal? iResult = dtc.sp_Logs_Insert(sTitle, sMessage, Type, DateTime.Now, AccountId, UserId).FirstOrDefault();
            }
        }

        public static void LogSystem(string sTitle, string sMessage, int AccountId, int UserId)
        {
            Log(sTitle, sMessage, 1, AccountId, UserId);
        }

        public static void LogError(Exception ex, int AccountId, int UserId)
        {
            string sError = ex.ToString();
            if (ex.InnerException != null) sError += ex.InnerException.ToString();
            Log(ex.Message, sError, 5, AccountId, UserId);
        }

        public static void LogInsert(string sTitle, string sMessage, int AccountId)
        {
            Log(sTitle, sMessage, 2, AccountId, 0);
        }

        public static void LogUpdate(string sTitle, string sMessage, int AccountId)
        {
            Log(sTitle, sMessage, 3, AccountId, 0);
        }

        public static void LogDelete(string sTitle, string sMessage, int AccountId)
        {
            Log(sTitle, sMessage, 4, AccountId, 0);
        }
    }
}