﻿using System;

namespace ShootingZombie_Backend.Content.Logic
{
    public class UrlHelper
    {
        public static string getRequestParam(string paramValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(paramValue) || string.IsNullOrWhiteSpace(paramValue))
                return defaultValue;
            return paramValue;
        }
        public static int getRequestParam(string paramValue, int defaultValue)
        {
            if (string.IsNullOrEmpty(paramValue))
                return defaultValue;
            return Convert.ToInt16(paramValue);
        }
    }
}