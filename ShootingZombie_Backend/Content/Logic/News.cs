﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShootingZombie_Backend.Content.Logic
{
    public class News
    {
        public int NewsId { get; set; }
        public int NewsGroupId { get; set; }
        public string Title { get; set; }
        public string Headline { get; set; }
        public string Alias { get; set; }
        public string Story { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }

    }
}