﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShootingZombie_Backend.Content.Logic
{
    public class Constants
    {
        public static string PAGE_TITLE = "Login - Shooting Zombie";

        public static void updateTitle(string sTitle)
        {
            PAGE_TITLE = sTitle;
        }
    }
}