﻿(function () {
    zombie.module.News.EditNewsGroup = function () {

        var validator = $("#frmEditNewsGroup").kendoValidator().data("kendoValidator");

        this.GetNewsGroupById = function (newsGroupId) {
            $('#newsGroupId').val(newsGroupId);
            $.ajax({
                url: "/DesktopModule/News/Service.asmx/NewsGroupGetById",
                data: JSON.stringify({ NewsGroupId : newsGroupId}),
                success: function (msg) {
                    if (msg.d == '-2') {
                        curModule.notification('Access is denied', 'error');
                    } else if (msg.d == '-1') {
                        curModule.notification('Error! Try again!', 'error');
                    } else {
                        var iNewsGroup = JSON.parse(msg.d);
                        $('#ENG_txtName').val(iNewsGroup.Name);
                        $('#ENG_txtAlias').val(iNewsGroup.Alias);
                        $('#ENG_txtStatus').data('kendoDropDownList').value(iNewsGroup.Status);
                    }
                }
            })
        }

        function defineElement() {
            $('#ENG_txtStatus').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Active', Value: 1 },
                    { Name: 'Hidden', Value: 2 }
                ],
                autoWidth: true
            });
        }

        function defineEventHandler() {
            $('#ENG_btnCancel').click(function (e) {
                $(this).closest("[data-role=window]").data("kendoWindow").close();
            });

            $('#ENG_btnEdit').click(function (e) {
                e.preventDefault();
                if (validator.validate()) {
                    $('#preloader').fadeIn(300);
                    var formData = $('#frmEditNewsGroup').serializeArray();
                    $.ajax({
                        url: "/DesktopModule/News/Service.asmx/UpdateNewsGroup",
                        data: JSON.stringify({ formData: formData }),
                        success: function (msg) {
                            $('#preloader').fadeOut(300);
                            if (msg.d == "-2")
                                curModule.notification('Access is denied', 'error');
                            else if (msg.d == '-1')
                                curModule.notification('Error! Try again!', 'error');
                            else if(msg.d == "0")
                                curModule.notification('Edit Newsgroup not success', 'error');
                            else {
                                curModule.notification('Edit Newsgroup successfully', 'info');
                                curModule.reload();
                                $('#ENG_btnCancel').trigger('click');
                                $('#frmEditNewsGroup').trigger('reset');
                            }
                        }
                    })
                }
            });

            $('#ENG_txtName').on('keyup', function (e) {
                var strAlias = curModule.ConvertToUnsign($(this).val());
                strAlias = strAlias.replace(/ /g, "-");
                $('#ENG_txtAlias').val(strAlias);
            });
        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);
    }
})();