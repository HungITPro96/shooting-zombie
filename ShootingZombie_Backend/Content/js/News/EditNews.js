﻿(function () {
    zombie.module.News.EditNews = function () {

        var validator = $("#frmEditNews").kendoValidator().data("kendoValidator");

        var EN_edtStory;

        this.initValue = function (NewsID) {
            $('#NewsId').val(NewsID);

            $.ajax({
                url: "/DesktopModule/News/Service.asmx/NewsGetById",
                data: JSON.stringify({ NewsId : NewsID}),
                success: function (msg) {
                    if (msg.d == "-2")
                        notificationMaster.show('Access is denied', 'error');
                    else if (msg.d == '-1')
                        notificationMaster.show('Error! Try again!', 'error');
                    else if(msg.d == '0')
                        notificationMaster.show('News not found', 'error');
                    else {
                        var iNews = JSON.parse(msg.d);
                        $('#EN_txtTitle').val(iNews.Title);
                        $('#EN_txtAlias').val(iNews.Alias);
                        $('#EN_txtHeadline').val(iNews.Headline);
                        EN_edtStory.setData(iNews.Story);
                        $('#EN_txtStatus').data('kendoDropDownList').value(iNews.Status);
                        $('#EN_txtType').data('kendoDropDownList').value(iNews.Type);
                    }
                }
            })
        }

        function defineElement() {
            $('#EN_txtStatus').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Active', Value: 1 },
                    { Name: 'Hidden', Value: 2 }
                ],
                autoWidth: true
            });
            $('#EN_txtType').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Normal News', Value: 1 },
                    { Name: 'Banner News', Value: 2 }
                ],
                autoWidth: true
            });

            EN_edtStory = CKEDITOR.replace('EN_txtStory');

        }

        function defineEventHandler() {
            $('#EN_btnCancel').click(function (e) {
                $(this).closest("[data-role=window]").data("kendoWindow").close();
            });

            $('#EN_txtTitle').on('keyup', function (e) {
                var strAlias = curModule.ConvertToUnsign($(this).val());
                strAlias = strAlias.replace(/ /g, "-");
                $('#EN_txtAlias').val(strAlias);
            });

            $('#frmEditNews').submit(function (e) {
                e.preventDefault();
                if (validator.validate()) {
                    $('#preloader').fadeIn(300);
                    var formData = $('#frmEditNews').serializeArray();
                    var iNews = {
                        NewsId: $('#NewsId').val(),
                        NewsGroupId: $('input#NewsGroupId').val(),
                        Title: $('input#EN_txtTitle').val(),
                        Headline: $('input#EN_txtHeadline').val(),
                        Alias: $('input#EN_txtAlias').val(),
                        Story: EN_edtStory.getData(),
                        Status: $('input#EN_txtStatus').val(),
                        Type: $('input#EN_txtType').val()
                    };
                    $.ajax({
                        url: "/DesktopModule/News/Service.asmx/UpdateNews",
                        data: JSON.stringify({ iNews: iNews }),
                        success: function (msg) {
                            $('#preloader').fadeOut(300);
                            if (msg.d == "-2")
                                curModule.notification('Access is denied', 'error');
                            else if (msg.d == '-1')
                                curModule.notification('Error! Try again!', 'error');
                            else if (msg.d == "0")
                                curModule.notification('Update News not success', 'error');
                            else {
                                curModule.notification('Update News successfully', 'info');
                                curModule.reload();
                                $('#EN_btnCancel').trigger('click');
                                $('#frmEditNews').trigger('reset');
                            }
                        }
                    });
                }
            })
        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);

    }
})()