﻿(function(){
    zombie.module.News.AddNewsGroup = function () {

        var validator = $("#frmAddNewsGroup").kendoValidator().data("kendoValidator");

        this.resetForm = function () {
            $('#NG_txtName').val('');
            $('#NG_txtAlias').val('');
            $('#NG_txtStatus').data('kendoDropDownList').value(1);
            $('span.k-tooltip-validation.k-invalid-msg').remove();
        }

        this.initValue = function (parentName, parentId) {
            $('input#parentId').val(parentId);
            if (parentId == 0) parentName = "Root Newsgroup";

            $('span#parentName').text('Parent name: ' + parentName);
        };

        function defineElement() {
            $('#NG_txtStatus').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Active', Value: 1 },
                    { Name: 'Hidden', Value: 2 }
                ],
                autoWidth: true
            })
        }
        
        function defineEventHandler() {
            $('#NG_btnCancel').click(function (e) {
                $(this).closest("[data-role=window]").data("kendoWindow").close();
            });
            
            $('#frmAddNewsGroup').submit(function (e) {
                e.preventDefault();
                if (validator.validate()) {
                    $('#preloader').fadeIn(300);
                    var formData = $('#frmAddNewsGroup').serializeArray();
                    $.ajax({
                        url: "/DesktopModule/News/Service.asmx/AddNewsGroup",
                        data: JSON.stringify({ formData: formData }),
                        success: function (msg) {
                            $('#preloader').fadeOut(300);
                            if (msg.d == "-2")
                                curModule.notification('Access is denied', 'error');
                            else if (msg.d == '-1')
                                curModule.notification('Error! Try again!', 'error');
                            else if(msg.d == "0")
                                curModule.notification('Add Newsgroup not success', 'error');
                            else {
                                curModule.notification('Add Newsgroup successfully', 'info');
                                curModule.reload();
                                $('#NG_btnCancel').trigger('click');
                                $('#frmAddNewsGroup').trigger('reset');
                            }
                        }
                    });
                } else {

                }
            });

            $('#NG_txtName').on('keyup', function (e) {
                var strAlias = curModule.ConvertToUnsign($(this).val());
                strAlias = strAlias.replace(/ /g, "-");
                $('#NG_txtAlias').val(strAlias);
            });
        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);
    }

})()