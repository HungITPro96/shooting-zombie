﻿(function () {
    zombie.module.News = function () {

        var wAddNews, wAddNewsGroup, wEditNews, wEditNewsGroup;
        var objAddNews, objAddNewsGroup, objEditNews, objEditNewsGroup;
        var dsNewsGroup;
        this.isTreeRoot = true;
        var treeViewNewsGroup;
        var notificationMaster;

        var tblMain, tblMainData;
        var totalRows, iRowPerPage = 10;

        var isEditNewsGroupMode = false;

        var lstNewsId = '';

        this.notification = function (sContent, sType) {
            notificationMaster.show(sContent, sType);
        }

        this.ConvertToUnsign = function ConvertToUnsign(str) {
            str = str.toLowerCase();
            str = str.replace(/[$\\@\\\#%\^\&\*\(\)\[\]\+\_\{\}\`\~\=\|/?:"“”',.!*]/g, "");
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            while (str.indexOf("  ") > 0) {
                str = str.replace(/  /g, ' ');
            }
            if (str[str.length - 1] == ' ')
                str = str.slice(0, str.length - 1);
            return str;
        }

        this.reload = function () {
            //var selectedNode = treeViewNewsGroup.parent(treeViewNewsGroup.select());
            //if (selectedNode.length > 0) {
            //    var selectedDataItem = treeViewNewsGroup.dataItem(selectedNode);
            //    selectedDataItem.loaded(false);
            //    selectedDataItem.load();
            //} else {
            //    treeViewNewsGroup.dataSource.read();
            //}
            //treeViewNewsGroup.expand(selectedNode);
            treeViewNewsGroup.dataSource.read();

        }

        this.UpdateStatusNewsGroup = function (lstNewsGroup, status) {
            $.ajax({
                url: "/DesktopModule/News/Service.asmx/UpdateNewsGroupStatus",
                data: JSON.stringify({ NewsGroupId: lstNewsGroup, Status: status }),
                success: function (msg) {
                    if(msg.d == "-2")
                        notificationMaster.show('Access is denied', 'error');
                    else if (msg.d == '-1')
                        notificationMaster.show('Error! Try again!', 'error');
                    else if (msg.d == "0")
                        notificationMaster.show('Add Newsgroup not success', 'error');
                    else {
                        notificationMaster.show('Change status success', 'info');
                    }
                    treeViewNewsGroup.dataSource.read();
                }
            });
        }

        this.checkAll = function () {
            $('input.Check-item').each(function () {
                this.checked = ($("input.Check-All:checked").length === 1 ? true : false);
                if (this.checked)
                    $(this).closest('tr').addClass("k-state-selected");
                else
                    $(this).closest('tr').removeClass("k-state-selected")
            });
            checkedMultiple();
        }

        function checkedMultiple() {
            lstNewsId = '';
            var countSelected = 0;
            $('input.Check-item').each(function () {
                if (this.checked) {
                    countSelected++;
                    lstNewsId += $(this).attr('ref') + ',';
                }
            });
            if (lstNewsId.length > 0)
                lstNewsId = lstNewsId.substr(0, lstNewsId.length-1);

            if (countSelected >= 2) {
                enableModiferElement(true);
            } else enableModiferElement(false);
        }

        function enableModiferElement(status) {
            if (status)
                $('button#btnDeleteNews').show();
            else 
                $('button#btnDeleteNews').hide();
        }

        function defineElement() {
            wAddNews = $('#addNews');
            wAddNewsGroup = $('#addNewsGroup');
            wEditNews = $('#editNews');
            wEditNewsGroup = $('#editNewsGroup')

            dsNewsGroup = new kendo.data.HierarchicalDataSource({
                transport: {
                    read: {
                        url: "/DesktopModule/News/Service.asmx/ListTreeNewsGroup"
                    },
                    parameterMap: function (data, operation) {
                        if (!data.NewsGroupId) data.NewsGroupId = parseInt(0);
                        if (data.NewsGroupId == 0) isTreeRoot = true;
                        return JSON.stringify(data);
                    }
                },
                schema: {
                    data: function (response) {
                        var arr = eval(response.d);
                        return arr;
                    },
                    model: {
                        id: "NewsGroupId",
                        hasChildren: "IsParent"
                    }
                }
            });

            treeViewNewsGroup = $('#treeViewNewsGroup').kendoTreeView({
                template: kendo.template($("#treeview-template").html()),
                dataSource: dsNewsGroup,
                autoBind: true,
                dataTextField: "Name",
                change: function (e) {
                    e.preventDefault();
                    $('h3.newsGroupName label').text(this.text(this.select()));
                    $('h3.newsGroupName').attr('rel', this.dataItem(this.select()).id);
                    $("h4#rootNode span").removeClass("label-primary").addClass("label-default");
                    tblMain.dataSource.page(1);

                },
                dataBound: function (e) {
                    if (isEditNewsGroupMode) {
                        $("#treeViewNewsGroup a.glyphicon").show();
                    }
                    if (isTreeRoot) {
                        $("h4#rootNode span").removeClass("label-primary").addClass("label-default");
                        var iDataItem = this.dataItem(".k-item:eq(0)");
                        if (iDataItem != undefined) {
                            var iNode = this.findByUid(iDataItem.uid);
                            if (iNode) {
                                this.expand(iNode);
                                this.select(iNode);
                            }
                            e.preventDefault();
                            isTreeRoot = false;
                        } else {
                            $("#rootNode").css('display', 'block');
                        }
                    }
                }
            }).data('kendoTreeView');

            notificationMaster = $('#notification').kendoNotification({
                position: {
                    top: 55,
                    left: Math.floor($(window).width() / 2 - 100)
                },
            }).data("kendoNotification");

            dsNews = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/DesktopModule/News/Service.asmx/NewsList",
                    },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            if (treeViewNewsGroup.dataItem(treeViewNewsGroup.select()) != undefined)
                                data.iNewsGroupId = parseInt(treeViewNewsGroup.dataItem(treeViewNewsGroup.select()).id);
                            else
                                data.iNewsGroupId = 0;

                            var now = new Date();
                            data.startDate = kendo.toString(new Date(new Date().getTime() - (30 * 24 * 60 * 60 * 1000)),"dd/MM/yyyy");
                            data.endDate = kendo.toString(new Date(), "dd/MM/yyyy");
                            data.status = 0;
                            data.keyword = '';// $('#iSearch').val();

                            return JSON.stringify(data);
                        }
                    }
                },
                schema: {
                    data: function (response) {
                        return tblMainData;
                    },
                    total: function (response) {
                        return totalRows;
                    }
                },
                serverSorting: true,
                sort: {},
                serverPaging: true,
                pageSize: iRowPerPage,
                requestEnd: function (e) {
                    if (typeof (e.response) != "undefined") {
                        tblMainData = JSON.parse(e.response.d);
                        if (tblMainData.length > 0)
                            totalRows = tblMainData[0]["TotalRows"];
                        else totalRows = 0;
                    }
                }
            })

            tblMain = $('#tblMain').kendoGrid({
                dataSource: dsNews,
                autoBind: false,
                sortable: {
                    mode: "multiple",
                    allowUnsort: true
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                resizable: true,
                rowTemplate: kendo.template($("#rowTemplate").html()),
                toolbar: kendo.template($("#toolbarTemplate").html()),
                columns: [
                        { command: [], title: '<input type="checkbox" id="cbkAll" class="Check-All" onclick="curModule.checkAll();"/>', width: "5%", headerAttributes: { style: "text-align: center;" } },
                        { field: "NewsId", title: "ID", width: "5%" },
                        { field: "Title", title: "Title", width: "15%" },
                        { field: "Story", title: "Story", width: "20%" },
                        { field: "Status", title: "Status", width: "15%" },
                        { field: "Type", title: "Type", width: "15%" }
                ]
            }).data('kendoGrid');

            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
        }

        function defineEventHandler() {
            
                $('#btnAddNews').click(function (e) {
                    e.preventDefault();
                    var NewsGroupId = parseInt($('h3.newsGroupName').attr('rel'));
                    if (NewsGroupId > 0) {
                        if (wAddNews.data('kendoWindow') == undefined) {
                            wAddNews = $('#addNews').kendoWindow({
                                width: "780px;", title: "Add News", content: "Module.aspx?m=News_AddNews", actions: ["Maximize", "Close"], modal: true, visible: false,
                                refresh: function (e) {
                                    objAddNews = objAddNews || new zombie.module.News.AddNews();
                                    objAddNews.initValue(NewsGroupId);
                                    wAddNews.data("kendoWindow").center().open();
                                }
                            });
                        } else {
                            objAddNews.resetForm();
                            objAddNews.initValue(NewsGroupId);
                            wAddNews.data("kendoWindow").center().open();
                        }
                    } else {
                        notificationMaster.show('Select NewsGroup first', 'warning');
                    }
                });
            

            $('#btnAddNewsGroup').click(function (e) {
                if ($('h3.newsGroupName').attr('rel') != '') {
                    if (wAddNewsGroup.data('kendoWindow') == undefined) {
                        wAddNewsGroup = $('#addNewsGroup').kendoWindow({
                            width: "600px;", title: "Add NewsGroup", content: "Module.aspx?m=News_AddNewsGroup", actions: ["Maximize", "Close"], modal: true, visible: false,
                            refresh: function (e) {
                                objAddNewsGroup = objAddNewsGroup || new zombie.module.News.AddNewsGroup();
                                objAddNewsGroup.initValue($('h3.newsGroupName label').text(), $('h3.newsGroupName').attr('rel'));
                                wAddNewsGroup.data('kendoWindow').center().open();
                            }
                        });
                    } else {
                        objAddNewsGroup.resetForm();
                        objAddNewsGroup.initValue($('h3.newsGroupName label').text(), $('h3.newsGroupName').attr('rel'));
                        wAddNewsGroup.data('kendoWindow').center().open();
                    }
                } else
                    notificationMaster.show('Select NewsGroup parent first', 'warning');
            });

            $('#btnEditNewsGroup').click(function (e) {
                e.preventDefault();
                if (isEditNewsGroupMode) {
                    $('#btnAddNewsGroup').fadeIn();
                    $(this).html('<i class="glyphicon glyphicon-pencil"></i>&nbsp;Edit');
                    $('#treeViewNewsGroup a.glyphicon').slideUp();
                    isEditNewsGroupMode = false;
                } else {
                    $('#btnAddNewsGroup').fadeOut();
                    $(this).html('<i class="glyphicon glyphicon-ok"></i>&nbsp;Done');
                    $('#treeViewNewsGroup a.glyphicon').slideDown();
                    isEditNewsGroupMode = true;
                }
            })

            $("h4#rootNode").click(function () {
                if ($("h4#rootNode span").hasClass("label-default")) {
                    $("h4#rootNode span").addClass("label-primary").removeClass("label-default");
                    $('h3.newsGroupName').attr('rel', 0);
                    $('h3.newsGroupName label').text('Root category');
                    try {
                        treeViewNewsGroup.select(null);
                    }
                    catch (e) {
                    }
                }
            });

            $(document).on('click', '#treeViewNewsGroup a.glyphicon-ok-circle', function (e) {
                var id = $(this).attr('ref');
                curModule.UpdateStatusNewsGroup(id, 2);
            });

            $(document).on('click', '#treeViewNewsGroup a.glyphicon-ban-circle', function (e) {
                var id = $(this).attr('ref');
                curModule.UpdateStatusNewsGroup(id, 1);
            });

            $(document).on('click', '#treeViewNewsGroup a.glyphicon-trash', function (e) {
                var newsGroupId = $(this).attr('ref');
                $.ajax({
                    url: "/DesktopModule/News/Service.asmx/NewsGroupCheckDeleteAvailable",
                    data: JSON.stringify({ NewsGroupId: newsGroupId }),
                    success: function (msg) {
                        if (msg.d == '-2') {
                            notificationMaster.show('Access is denied', 'error');
                        } else if (msg.d == '-1') {
                            notificationMaster.show('Error! Try again!', 'error');
                        } else if (msg.d == '0') {
                            notificationMaster.show('Can not delete this NewsGroup because have News relative', 'error');
                        } else if (msg.d == '1') {
                            if (confirm("Do you want to delete this News group?")) {
                                $.ajax({
                                    url: "/DesktopModule/News/Service.asmx/NewsGroupDelete",
                                    data: JSON.stringify({ NewsGroupId: newsGroupId }),
                                    success: function (msg) {
                                        if (msg.d == '-2') {
                                            notificationMaster.show('Access is denied', 'error');
                                        } else if (msg.d == '-1') {
                                            notificationMaster.show('Error! Try again!', 'error');
                                        } else if (msg.d == '0') {
                                            notificationMaster.show('Can not delete this NewsGroup because have News relative', 'error');
                                        } else {
                                            notificationMaster.show('Delete successfully', 'info');
                                            treeViewNewsGroup.dataSource.read();
                                        }
                                    }

                                })
                            }
                        }
                    }
                })
            });

            $(document).on('click', '#treeViewNewsGroup a.glyphicon-edit', function (e) {
                e.preventDefault();
                var newsGroupId = $(this).attr('ref');
                if (wEditNewsGroup.data('kendoWindow') == undefined) {
                    wEditNewsGroup = $('#editNewsGroup').kendoWindow({
                        width: "600px;", title: "Edit NewsGroup", content: "Module.aspx?m=News_EditNewsGroup", actions: ["Maximize", "Close"], modal: true, visible: false,
                        refresh: function (e) {
                            objEditNewsGroup = objEditNewsGroup || new zombie.module.News.EditNewsGroup();
                            objEditNewsGroup.GetNewsGroupById(newsGroupId);
                            wEditNewsGroup.data('kendoWindow').center().open();
                        }
                    })
                } else {
                    objEditNewsGroup.GetNewsGroupById(newsGroupId);
                    wEditNewsGroup.data('kendoWindow').center().open();
                }

            });

            $(document).on('click', '#tblMain a.news-title', function (e) {
                var newsId = $(this).attr('ref');
                if (wEditNews.data('kendoWindow') == undefined) {
                    wEditNews = $('#editNews').kendoWindow({
                        width: "780px;", title: "Edit News", content: "Module.aspx?m=News_EditNews", actions: ["Maximize", "Close"], modal: true, visible: false,
                        refresh: function (e) {
                            objEditNews = objEditNews || new zombie.module.News.EditNews();
                            objEditNews.initValue(newsId);
                            wEditNews.data('kendoWindow').center().open();
                        }
                    })
                } else {
                    objEditNews.initValue(newsId);
                    wEditNews.data('kendoWindow').center().open();
                }
            });

            $(document).on('click', '#tblMain .Check-item', function (e) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass("k-state-selected");
                } else {
                    $(this).closest('tr').removeClass("k-state-selected");
                }
                var checkAll = true;
                $('input.Check-item').each(function () {
                    if (!this.checked) {
                        checkAll = false;
                        return;
                    }
                });
                $('input#cbkAll').prop('checked', checkAll);
                checkedMultiple();
            });

            $('button#btnDeleteNews').click(function (e) {
                if (lstNewsId.length > 0) {
                    $('#preloader').fadeIn(300);
                    $.ajax({
                        url: '',
                        data: JSON.stringify({ lstNewsId: lstNewsId }),
                        success: function (msg) {
                            $('#preloader').fadeOut(300);
                            if (msg.d == '-2') {
                                notificationMaster.show('Access is denied', 'error');
                            } else if (msg.d == '-1') {
                                notificationMaster.show('Error! Try again!', 'error');
                            } else {
                                notificationMaster.show('Delete successfully', 'info');
                                dsNews.read();
                            }
                        },
                        error: function (msg) {
                            $('#preloader').fadeOut(300);
                            notificationMaster.show('Error! Try again!', 'error');
                        }
                    })
                } else {
                    notificationMaster.show('Select News first', 'warning');
                }
            })
        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);
    }
})();

curModule = new zombie.module.News();