﻿(function () {
    zombie.module.News.AddNews = function () {

        var validator = $("#frmAddNews").kendoValidator().data("kendoValidator");
        var edtStory;

        this.resetForm = function () {
            $('#N_txtTitle').val('');
            $('#N_txtAlias').val('');
            $('#N_txtHeadline').val('');
            edtStory.setData('');
            $('#N_txtStatus').data('kendoDropDownList').value(1);
            $('#N_txtType').data('kendoDropDownList').value(1);
            $('span.k-tooltip-validation.k-invalid-msg').remove();
        }

        this.initValue = function (NewsGroupId) {
            $('input#NewsGroupId').val(NewsGroupId);
        };

        function defineElement() {
            

            $('#N_txtStatus').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Active', Value: 1 },
                    { Name: 'Hidden', Value: 2 }
                ],
                autoWidth: true
            });
            $('#N_txtType').kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: [
                    { Name: 'Normal News', Value: 1 },
                    { Name: 'Banner News', Value: 2 }
                ],
                autoWidth: true
            });

            edtStory = CKEDITOR.replace('N_txtStory');
            CKFinder.setupCKEditor(edtStory, '/Content/ckfinder');
        }

        function defineEventHandler() {

            $('#N_btnCancel').click(function (e) {
                $(this).closest("[data-role=window]").data("kendoWindow").close();
            });

            $('#N_txtTitle').on('keyup', function (e) {
                var strAlias = curModule.ConvertToUnsign($(this).val());
                strAlias = strAlias.replace(/ /g, "-");
                $('#N_txtAlias').val(strAlias);
            });

            $('#frmAddNews').submit(function (e) {
                e.preventDefault();
                if (validator.validate()) {
                    $('#preloader').fadeIn(300);
                    var iNews = {
                        NewsId : 0,
                        NewsGroupId: $('input#NewsGroupId').val(),
                        Title: $('input#N_txtTitle').val(),
                        Headline: $('input#N_txtHeadline').val(),
                        Alias: $('input#N_txtAlias').val(),
                        Story: edtStory.getData(),
                        Status: $('input#N_txtStatus').val(),
                        Type: $('input#N_txtType').val()
                    }
                    $.ajax({
                        url: "/DesktopModule/News/Service.asmx/AddNews",
                        data: JSON.stringify({ iNews: iNews }),
                        success: function (msg) {
                            $('#preloader').fadeOut(300);
                            if (msg.d == "-2")
                                curModule.notification('Access is denied', 'error');
                            else if (msg.d == '-1')
                                curModule.notification('Error! Try again!', 'error');
                            else if (msg.d == "0")
                                curModule.notification('Add News not success', 'error');
                            else {
                                curModule.notification('Add News successfully', 'info');
                                curModule.reload();
                                $('#N_btnCancel').trigger('click');
                                $('#frmAddNews').trigger('reset');
                            }
                        }
                    });
                } else {

                }
            });

            $(document).on('click', '.del-pic', function (e) {
                var ref = $(this).attr('ref');
                var rel = $(this).attr('rel');
                $('#' + ref).attr('src', '/Content/images/default.png');
                $('#' + rel).val('');
            });

            $(document).on('click', '.add-pic', function (e) {
                var ref = $(this).attr('ref');
                var rel = $(this).attr('rel');
                var ckfinder = new CKFinder();
                ckfinder.filebrowserImageBrowseUrl = function (fileUrl) {
                    $('#' + ref).attr('src', fileUrl);
                    $('#' + rel).val(fileUrl.replace("http://localhost", ""));
                }
                ckfinder.popup();
            });
        }

        (function () {
            defineElement();
            defineEventHandler();
        }).call(this);
    }
})()