﻿(function () {
    zombie.module.Log = function () {

        var tblLog;
        var LogDataSource;
        var tblLogData;
        var totalRows;
        var iRowPerPage = 10;

        var notificationMaster;

        var objPreview;
        var wPreview = $('#Preview');

        this.notification = function (sContent, sType) {
            notificationMaster.show(sContent, sType);
        }

        function defineElement() {

            var curTime = new Date();
            $('#startTime').kendoDatePicker({
                format: "dd/MM/yyyy",
                value: new Date(curTime.getFullYear(), curTime.getMonth(), 1),
                max: curTime,
                change: function (e) {
                    $('#endTime').data('kendoDatePicker').min(this.value());
                    LogDataSource.read();
                }
            });

            $('#endTime').kendoDatePicker({
                format: "dd/MM/yyyy",
                value: curTime,
                min: new Date(curTime.getFullYear(), curTime.getMonth(), 1),
                change: function (e) {
                    $('#startTime').data('kendoDatePicker').max(this.value());
                    LogDataSource.read();
                }
            });

            $('#logType').kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true,
                dataSource: [
                    //1: System, 2: Insert, 3: Update, 4: Delete, 5: Error
                    { text: 'All', value: -1 },
                    { text: 'System', value: 1 },
                    { text: 'Insert', value: 2 },
                    { text: 'Update', value: 3 },
                    { text: 'Delete', value: 4 },
                    { text: 'Error', value: 5 }
                ],
                template: kendo.template($("#template_LogType").html()),
                change: function (e) {
                    LogDataSource.read();
                }
            });

            $('#logStatus').kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true,
                dataSource: [
                    //1: System, 2: Insert, 3: Update, 4: Delete, 5: Error
                    { text: 'All', value: -1 },
                    { text: 'Undone', value: 1 },
                    { text: 'Done', value: 2 },
                ],
                template: kendo.template($("#template_LogStatus").html()),
                change: function (e) {
                    LogDataSource.read();
                }
            });

            LogDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/DesktopModule/Log/Service.asmx/LogList",
                    },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            data.startDate = $('#startTime').data('kendoDatePicker').value();
                            var end = $('#endTime').data('kendoDatePicker').value();
                            data.endDate = new Date(end.getFullYear(), end.getMonth(), end.getDate(), 23, 59, 59);
                            data.type = $('#logType').data('kendoDropDownList').value();
                            data.status = $('#logStatus').data('kendoDropDownList').value();
                            data.GetByAccount = 3;
                            return JSON.stringify(data);
                        }
                    }
                },
                schema: {
                    data: function (response) {
                        return tblLogData;
                    },
                    total: function (response) {
                        return totalRows;
                    }
                },
                serverSorting: true,
                sort: {},
                serverPaging: true,
                pageSize: iRowPerPage,
                requestEnd: function (e) {
                    if (typeof (e.response) != "undefined") {
                        tblLogData = JSON.parse(e.response.d);
                        if (tblLogData.length > 0)
                            totalRows = tblLogData[0]["TotalRows"];
                        else totalRows = 0;
                    }
                }
            });

            tblLog = $('#tblLog').kendoGrid({
                dataSource: LogDataSource,
                autoBind: false,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                selectable: false,
                sortable:true,
                rowTemplate: kendo.template($("#rowTemplate").html()),
                columns: [
                        { field: "LogId", title: "Id", width: "5%" },
                        { field: "Title", title: "Title", width: "30%" },
                        { field: "Message", title: "Message" },
                        { field: "DateCreated", title: "Date Created", width: "15%" },
                        { field: "Name", title: "Name", width: "10%" },
                        { command: [], title: "Status", width: "100px", attributes: {"class": "text-center"} }
                ]
            }).data('kendoGrid');

            notificationMaster = $('#notification').kendoNotification({
                position: {
                    top: 55,
                    left: Math.floor($(window).width() / 2 - 100)
                },
            }).data("kendoNotification");
            
        }

        function defineEventHandler() {
            $(document).on('click', '#tblLog a.title', function (e) {
                e.preventDefault();
                var id = $(this).attr('rel');
                if (wPreview.data('kendoWindow') == undefined) {
                    wPreview = $('#Preview').kendoWindow({
                        width: "800px;", title: "Log Detail", content: "Module.aspx?m=Log_PreviewLog", actions: ["Maximize", "Close"], modal: true, visible: false,
                        refresh: function (e) {
                            objPreview = objPreview || new zombie.module.Log.PreviewLog();
                            objPreview.InitData(id);
                            wPreview.data('kendoWindow').center().open();
                        }
                    })
                } else {
                    objPreview.InitData(id);
                    wPreview.data('kendoWindow').center().open();
                }
            });

            $(document).on('click', 'a.mark-done', function (e) {
                e.preventDefault();
                var id = $(this).attr('ref');
                $.ajax({
                    url: "/DesktopModule/Log/Service.asmx/LogUpdateStatus",
                    data: JSON.stringify({ LogId: id, Status: 2 }),
                    success: function (msg) {
                        if (msg.d == "-2")
                            curModule.notification('Access is denied', 'error');
                        else if (msg.d == '-1')
                            curModule.notification('Error! Try again!', 'error');
                        else if (msg.d == "0")
                            curModule.notification('Update status not success', 'error');
                        else {
                            curModule.notification('Update status successfuly', 'info');
                            LogDataSource.read();
                        }
                    }
                })
            })
        }

        (function () {
            defineElement();
            defineEventHandler();
            LogDataSource.read();
        }).call(this);

    }
})();

curModule = new zombie.module.Log();

