﻿using ShootingZombie_Backend.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShootingZombie_Backend.Content.Controller
{
    public partial class TopMenu : System.Web.UI.UserControl
    {
        protected string sFullName;
        private string[] lstPermission = null;
        protected string sModule;
        protected void Page_Load(object sender, EventArgs e)
        {
            sFullName = UrlHelper.getRequestParam((string)Session["z_FullName"], "");
            lstPermission = (string[])Session["z_ListPermission"];
            sModule = UrlHelper.getRequestParam(Request.Params["m"], "Dashboard");
        }
        public bool CheckPermission(string p)
        {
            return PermissionHelper.CheckPermission(lstPermission, p);
        }
    }
}