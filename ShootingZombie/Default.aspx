﻿<%@ Page Language="C#" MasterPageFile="~/Content/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ShootingZombie.Default" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="MasterPage_Top" runat="server"></asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:PlaceHolder runat="server" ID="contentPlaceHolder"></asp:PlaceHolder>

</asp:Content>

<asp:Content ID="FooterContent" ContentPlaceHolderID="MasterPage_Footer" runat="server">
    <% if(sModule=="Play") { %>
        <link href="<%=Page.ResolveClientUrl("~/Content/css/game.css") %>" rel="stylesheet" />
        <script type='text/javascript'>
            var Module = {
                TOTAL_MEMORY: 268435456,
                errorhandler: null,			// arguments: err, url, line. This function must return 'true' if the error is handled, otherwise 'false'
                compatibilitycheck: null,
                dataUrl: "<%=LinkHelper.Parse("/Content/GameData/Release/Final.data") %>",
                codeUrl: "<%=LinkHelper.Parse("/Content/GameData/Release/Final.js") %>",
                memUrl: "<%=LinkHelper.Parse("/Content/GameData/Release/Final.mem") %>",
            };
        </script>
        <script src="<%=LinkHelper.Parse("/Content/GameData/Release/UnityLoader.js") %>"></script>
    <%} else if(sModule=="Home") {%>
        <script type="text/javascript" src="<%=Page.ResolveClientUrl("/Content/js/responsiveslides.min.js") %>"></script>
    <%} %>
    <% if(existJsFile) { %>
        <script type="text/javascript" src="/Content/js/js_<%=sModule %>.js"></script>
    <%} %>
</asp:Content>
