﻿$(document).ready(function () {
    var validatorLogin = $('#frmLogin')
    validatorLogin.validate();
    $('#txtUserName_Login').rules("add", {
        required: true,
        messages: {
            required: "* Username is required"
        }
    });

    $('#txtPassword_Login').rules("add", {
        required: true,
        messages: {
            required: "* Password is required"
        }
    });

    $('#btnSubmitLogin').click(function (e) {
        $('#btnClick').val($(this).attr('id'));
        console.log(validatorLogin.valid());
        if (validatorLogin.valid()) {
            var $this = $(this);
            $(this).css('cursor', 'not-allowed');
            $this.button('loading');
        }
    });

    $('#txtPasswordRef_Register').on('keyup', function (e) {
        if ($(this).val() != $('#txtPassword_Register').val()) {
            $('#txtPasswordRef_Register-error').show();
            $('#txtPasswordRef_Register-error').text('* Password is not same');
        } else {
            $('#txtPasswordRef_Register-error').hide();
        }
    })

});