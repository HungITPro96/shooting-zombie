﻿$(document).ready(function () {
    var validatorLogin = $('#frmLogin')
    validatorLogin.validate();

    $('#txtUserName_Register').rules("add", {
        required: true,
        messages: {
            required: "* Username is required"
        }
    });

    $('#txtPassword_Register').rules("add", {
        required: true,
        minlength: 6,
        messages: {
            required: "* Password is required",
            minlength: "* Please enter at least 6 characters"
        }
    });

    $('#txtPasswordRef_Register').rules("add", {
        required: true,
        messages: {
            required: "* Password is required"
        }
    });

    $('#txtEmail_Register').rules("add", {
        required: true,
        email: true,
        messages: {
            required: "* Email is required",
            email: "* Please enter a valid email address"
        }
    });

    $('#btnSubmitRegister').click(function (e) {
        $('#btnClick').val($(this).attr('id'));
        if (validatorLogin.valid()) {
            if ($('#txtPassword_Register').val() != $('#txtPasswordRef_Register').val()) {
                e.preventDefault();
                $('#txtPasswordRef_Register-error').show();
                $('#txtPasswordRef_Register-error').text('* Password is not same');
            } else {
                var $this = $(this);
                $(this).css('cursor', 'not-allowed');
                $this.button('loading');
            }
        }
    });

    $('#txtPasswordRef_Register').on('keyup', function (e) {
        if ($(this).val() != $('#txtPassword_Register').val()) {
            $('#txtPasswordRef_Register-error').show();
            $('#txtPasswordRef_Register-error').text('* Password is not same');
        } else {
            $('#txtPasswordRef_Register-error').hide();
        }
    });

    $('#txtPasswordRef_Register').focusout(function (e) {
        if ($(this).val() != $('#txtPassword_Register').val()) {
            $('#txtPasswordRef_Register-error').show();
            $('#txtPasswordRef_Register-error').text('* Password is not same');
        } else {
            $('#txtPasswordRef_Register-error').hide();
        }   
    });



});