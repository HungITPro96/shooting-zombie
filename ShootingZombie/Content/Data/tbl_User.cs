//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShootingZombie.Content.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_User
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public Nullable<byte> Gender { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> LastLoginOn { get; set; }
        public Nullable<bool> IsOnline { get; set; }
        public Nullable<byte> Status { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<int> Experience { get; set; }
        public Nullable<int> Gold { get; set; }
    }
}
