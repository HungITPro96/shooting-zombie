//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShootingZombie.Content.Data
{
    using System;
    
    public partial class fesp_NewsGroups_ListNews_Result
    {
        public int NewsGroupId { get; set; }
        public string Name { get; set; }
        public string Lineage { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string Alias { get; set; }
    }
}
