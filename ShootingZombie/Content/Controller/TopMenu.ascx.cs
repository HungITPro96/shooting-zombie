﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Content.Controller
{
    public partial class TopMenu : System.Web.UI.UserControl
    {
        protected string sModule;
        protected string sFullName;
        protected void Page_Load(object sender, EventArgs e)
        {
            sModule = UrlHelper.getRequestParam(Request.Params["m"], "Home");
            sFullName = UrlHelper.getRequestParam(Session["z_FullName"], "");
        }

        protected string ActiveCurrentModule(string sModule)
        {
            string currentModule = UrlHelper.getRequestParam(Request.Params["m"], "Home");
            return (currentModule == sModule) ? "active" : "";
        }
    }
}