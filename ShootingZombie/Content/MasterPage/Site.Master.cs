﻿using ShootingZombie.Content.Logic;
using System;
using System.Web.UI;

namespace ShootingZombie
{
    public partial class SiteMaster : MasterPage
    {
        protected string sModule;
        protected void Page_Load(object sender, EventArgs e)
        {
            sModule = UrlHelper.getRequestParam(Request.Params["m"], "Home");
        }
    }
}