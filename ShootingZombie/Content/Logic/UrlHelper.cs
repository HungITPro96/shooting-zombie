﻿namespace ShootingZombie.Content.Logic
{
    public class UrlHelper
    {
        public static string getRequestParam(string paramValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(paramValue) || string.IsNullOrWhiteSpace(paramValue))
                return defaultValue;
            return paramValue;
        }
        public static string getRequestParam(object paramValue, string defaultValue)
        {
            if (paramValue != null)
                return paramValue.ToString();
            return defaultValue;
        }
        public static int getRequestParam(object paramValue, int defaultValue)
        {
            if (paramValue != null)
            {
                if(!paramValue.ToString().Equals(""))
                    return int.Parse(paramValue.ToString());
            }
            return defaultValue;
        }
    }
}