﻿namespace ShootingZombie.Content.Logic
{
    public class Constants
    {
        public static string PAGE_TITLE = "Trang chủ - Shooting Zombie";

        public static void updateTitle(string sTitle)
        {
            PAGE_TITLE = sTitle;
        }
    }
}