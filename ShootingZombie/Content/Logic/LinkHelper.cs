﻿using System.Web;

namespace ShootingZombie.Content.Logic
{
    public class LinkHelper
    {
        public static string Parse(string sUrl)
        {
            string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            string DOMAIN = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "");
            if (string.IsNullOrEmpty(sUrl)) return "";
            //if (DOMAIN.IndexOf("localhost") >= 0) return DOMAIN + sUrl;

            //return "";
            return DOMAIN + sUrl;

        }
        private static readonly string[] VietNamChar = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };

        public static string ToAlias(string str)
        {
            //Thay thế và lọc dấu từng char      
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            str = str.Replace(" ", "-");
            return str.ToLower();
        }
    }
}