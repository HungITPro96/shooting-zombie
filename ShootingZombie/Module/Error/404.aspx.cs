﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Error
{
    public partial class _404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Page not found - 404");
        }
    }
}