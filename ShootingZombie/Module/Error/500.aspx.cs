﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Error
{
    public partial class _500 : System.Web.UI.Page
    {
        protected string sError = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Error - 500");

            Exception exc = Server.GetLastError();
            if (exc != null)
            {
                sError = exc.Message + "<br/>";
                sError += exc.StackTrace + "<br/>";
                if (exc.InnerException != null)
                {
                    sError += exc.InnerException.Message + "<br/>";
                    sError += exc.InnerException.StackTrace + "<br/>";
                }
            }
            // Clear the error from the server.
            Server.ClearError();
        }
    }
}