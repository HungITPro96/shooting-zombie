﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace ShootingZombie.Module.News
{
    public partial class Category : System.Web.UI.UserControl
    {
        protected List<fesp_NewsGroups_ListNews_Result> lstNewsGroup;
        protected List<fesp_News_GetByNewsGroupId_Result> lstNews;
        protected string sCategory;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                sCategory = UrlHelper.getRequestParam(Request.Params["c"], "");

                using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                {
                    lstNewsGroup = dtc.fesp_NewsGroups_ListNews().ToList();
                    if(lstNewsGroup.Find(p => p.Alias == sCategory) != null)
                    {
                        int id = lstNewsGroup.Find(p => p.Alias == sCategory).NewsGroupId;
                        lstNews = dtc.fesp_News_GetByNewsGroupId(id).ToList();
                    }else
                    {
                        Response.Redirect("/?m=404");
                    }
                }
            }
            catch(Exception ex)
            {
                Response.Redirect("/?m=404");
            }
        }
    }
}