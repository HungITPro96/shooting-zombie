﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShootingZombie.Module.News
{
    public partial class News : System.Web.UI.UserControl
    {
        protected List<fesp_NewsGroups_ListNews_Result> lstNewsGroup;
        protected void Page_Load(object sender, EventArgs e)
        {

            Constants.updateTitle("News - Shooting Zombie");

            try
            {
                using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                {
                    lstNewsGroup = dtc.fesp_NewsGroups_ListNews().ToList();
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("/?m=404");
            }
        }

        protected List<fesp_News_GetByNewsGroupId_Result> NewsGetByNewsGroup(int NewsGroupId)
        {
            using(ShootingZombieEntities dtc = new ShootingZombieEntities())
            {
                return dtc.fesp_News_GetByNewsGroupId(NewsGroupId).ToList();
            }
        }
    }
}