﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Linq;

namespace ShootingZombie.Module.News
{
    public partial class Detail : System.Web.UI.UserControl
    {
        protected fesp_News_GetById_Result iNews;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sCategory = UrlHelper.getRequestParam(Request.Params["c"], "");
                string sItem = UrlHelper.getRequestParam(Request.Params["i"], "");

                int iPos = sItem.LastIndexOf('-');
                if (iPos > 0)
                {
                    string sNewsId = sItem.Substring(iPos+1);
                    string sAlias = sItem.Substring(0, iPos);
                    int NewsId = 0;
                    if (int.TryParse(sNewsId, out NewsId))
                    {
                        using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                        {
                            iNews = dtc.fesp_News_GetById(NewsId).FirstOrDefault();
                            if (iNews == null)
                            {
                                Response.Redirect("/?m=404");
                            }
                        }
                    }
                }else
                {
                    Response.Redirect("/?m=404");
                }
            }
            catch(Exception ex)
            {
                Response.Redirect("/?m=404");
            }

        }
    }
}