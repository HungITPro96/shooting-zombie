﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Category.ascx.cs" Inherits="ShootingZombie.Module.News.Category" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>
<%@ Import Namespace="ShootingZombie.Content.Data" %>

<div class="container container-top-newsDetail">
    <div class="container-newsDetail">
        <div class="newsDetail-breadcrumb panel newsgroup">
            <div class="panel-body">
                <ul class="nav nav-tabs responsive">
                    <% foreach (fesp_NewsGroups_ListNews_Result iNewsGroup in lstNewsGroup) {%>
                        <li <%if(iNewsGroup.Alias == sCategory) { %> class="active"<%} %>><a href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias) %>" title="<%=iNewsGroup.Name %>"><%=iNewsGroup.Name %></a></li>
                    <%} %>
                </ul>
            </div>
        </div>
        <div class="news-content news-category-content">
                <% int indexImage = 1; for (int i = 0; i < lstNews.Count; i++) {
                        fesp_News_GetByNewsGroupId_Result iNews = lstNews[i];
                        if (indexImage == 6) indexImage = 1;%>
                    <div class="category-news-section">
                        <div class="category-news-img">
                            <a href="<%=LinkHelper.Parse("/?m=News&c="+sCategory+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" title="<%=iNews.Title %>">
                                <img src="<%=Page.ResolveClientUrl("/Content/images/news/"+indexImage+".jpg") %>" width="100%"/> 
                            </a>
                        </div>
                        <div class="category-news-content">
                            <a class="top-news-title" href="<%=LinkHelper.Parse("/?m=News&c="+sCategory+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" title="<%=iNews.Title %>"><%=iNews.Title %></a>
                            <div class="top-news-headline"><%=iNews.Headline %></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <%indexImage++;} %>
        </div>
    </div>
</div>
