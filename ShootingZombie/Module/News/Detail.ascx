﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Detail.ascx.cs" Inherits="ShootingZombie.Module.News.Detail" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>

<div class="container container-top-newsDetail">
    <div class="container-newsDetail">
        <div class="row newsDetail-breadcrumb panel">
            <div class="panel-body">
                <div class="col-md-1">
                    <span class="glyphicon glyphicon-globe"></span>
                </div>
                <div class="col-md-11">
                    <ul class="breadcrumb">
                        <li><a href="<%=LinkHelper.Parse("/") %>" title="Home">Home</a></li>
                        <li><a href="<%=LinkHelper.Parse("/?m=News") %>" title="News">News</a></li>
                        <li><a href="<%=LinkHelper.Parse("/?m=News&c="+iNews.GroupAlias) %>" title="<%=iNews.GroupName %>"><%=iNews.GroupName %></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row newsDetail-content">
            <div class="newsDetail-header">
                <h3 class="newsDetail-title"><%=iNews.Title %></h3>
                <h4 class="newsDetail-datecreated"><%=iNews.CreateOn.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm") %></h4>
            </div>
            <div class="newsDetail-body">
                <p>
                    <%=iNews.Story %>
                </p>
            </div>
            <div class="newsDetail-footer">
                <div class="fb-like" 
                    data-href="<%=LinkHelper.Parse("/?m=News&c="+iNews.GroupAlias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" 
                    data-layout="button_count" 
                    data-action="like" 
                    data-show-faces="true">
                </div>
                <div class="fb-send" 
                    data-href="<%=LinkHelper.Parse("/?m=News&c="+iNews.GroupAlias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" 
                    data-layout="button_count"
                    data-size="small">
                    </div>
                <div class="fb-share-button" 
                    data-href="<%=LinkHelper.Parse("/?m=News&c="+iNews.GroupAlias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" 
                    data-layout="button_count">
                </div>
            </div>
            <div class="relative-newsDetail">
                <div class="relative-title">
                    <span>Related News</span>
                </div>
                <div class="relative-content">
                    <ul>
                        <li>
                            <a href="http://localhost:50989/?m=News&c=guide&i=truc-tiep-chung-ket-cfs-grand-final-2017-eva-team-vs-super-valiant-9">TRỰC TIẾP CHUNG KẾT CFS GRAND FINAL 2017: EVA TEAM VS SUPER VALIANT</a>
                        </li>
                        <li>
                            <a href="http://localhost:50989/?m=News&c=guide&i=truc-tiep-chung-ket-cfs-grand-final-2017-eva-team-vs-super-valiant-9">TRỰC TIẾP CHUNG KẾT CFS GRAND FINAL 2017: EVA TEAM VS SUPER VALIANT</a>
                        </li>
                        <li>
                            <a href="http://localhost:50989/?m=News&c=guide&i=truc-tiep-chung-ket-cfs-grand-final-2017-eva-team-vs-super-valiant-9">TRỰC TIẾP CHUNG KẾT CFS GRAND FINAL 2017: EVA TEAM VS SUPER VALIANT</a>
                        </li>
                        <li>
                            <a href="http://localhost:50989/?m=News&c=guide&i=truc-tiep-chung-ket-cfs-grand-final-2017-eva-team-vs-super-valiant-9">TRỰC TIẾP CHUNG KẾT CFS GRAND FINAL 2017: EVA TEAM VS SUPER VALIANT</a>
                        </li>
                        <li>
                            <a href="http://localhost:50989/?m=News&c=guide&i=truc-tiep-chung-ket-cfs-grand-final-2017-eva-team-vs-super-valiant-9">TRỰC TIẾP CHUNG KẾT CFS GRAND FINAL 2017: EVA TEAM VS SUPER VALIANT</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
