﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="ShootingZombie.Module.News.News" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>
<%@ Import Namespace="ShootingZombie.Content.Data" %>
<%@ Import Namespace="System.Collections.Generic" %>

<div class="container container-top-newsDetail">
    <div class="container-newsDetail">
        <div class="row newsDetail-breadcrumb panel">
            <div class="panel-body">
                <div class="col-md-1">
                    <span class="glyphicon glyphicon-globe"></span>
                </div>
                <div class="col-md-11">
                    <ul class="breadcrumb">
                        <li><a href="<%=LinkHelper.Parse("/") %>" title="Home">Home</a></li>
                        <li><a href="<%=LinkHelper.Parse("/?m=News") %>" title="News">News</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <%--<div class="newsDetail-breadcrumb panel newsgroup">
            <div class="panel-body">
                <ul class="nav nav-tabs responsive">
                    <% foreach (fesp_NewsGroups_ListNews_Result iNewsGroup in lstNewsGroup) {%>
                        <li <%if(lstNewsGroup.IndexOf(iNewsGroup)==0) { %> class="active"<%} %>><a href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias) %>" title="<%=iNewsGroup.Name %>"><%=iNewsGroup.Name %></a></li>
                    <%} %>
                </ul>
            </div>
        </div>--%>
        <div class="news-content">
            <% int indexImage = 1;
                for (int i = 0; i < lstNewsGroup.Count; i++) {
                    fesp_NewsGroups_ListNews_Result iNewsGroup = lstNewsGroup[i];
                    if (indexImage == 6) indexImage = 1; %>
                <% if(i%2==0) { %>
                    <div class="row">  
                <%} %>
                    <div class="col-md-6 col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h3 class="section-title">
                                        <a href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias)%>" title="<%=iNewsGroup.Name %>">
                                            <%=iNewsGroup.Name %>
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <div class="panel-body">
                                <% List<fesp_News_GetByNewsGroupId_Result> lstNews = NewsGetByNewsGroup(iNewsGroup.NewsGroupId);
                                    for (int j = 0; j < lstNews.Count; j++) {
                                        if (j > 3) break;
                                        fesp_News_GetByNewsGroupId_Result iNews = lstNews[j];
                                        if(j==0) {%>
                                            <div class="section-top-news">
                                                <div class="top-news-img">
                                                    <a href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>"><img src="<%=Page.ResolveClientUrl("/Content/images/news/"+indexImage+".jpg") %>" width="100%" height="100px"/></a>
                                                </div>
                                                <div class="top-news-content">
                                                    <a class="top-news-title" href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" title="<%=iNews.Title %>"><%=iNews.Title %></a>
                                                    <div class="top-news-headline"><%=iNews.Headline %></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                    <%} else {
                                            if(j==1) { %>
                                                <div class="section-news-other">
                                                    <ul>
                                            <%} %>
                                                    <li><a href="<%=LinkHelper.Parse("/?m=News&c="+iNewsGroup.Alias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>"><%=iNews.Title %></a></li> 
                                            <% if(j==3||j==lstNews.Count-1) { %>
                                                    </ul>
                                                </div>
                                    <%}} %>
                                <%} %>
                            </div>
                        </div>
                    </div>
                <% if(i%2==1 || i==lstNewsGroup.Count-1) { %>
                    </div>
                <%} %>
            <% indexImage++; } %>
        </div>
    </div>
</div>