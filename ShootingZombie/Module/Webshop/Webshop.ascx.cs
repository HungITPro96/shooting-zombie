﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Webshop
{
    public partial class Webshop : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Webshop - Shooting Zombie");
        }
    }
}