﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Game
{
    public partial class Game : System.Web.UI.UserControl
    {
        protected string sFullName;
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Play Now - Shooting Zombie");

            sFullName = UrlHelper.getRequestParam(Session["z_FullName"], "");
            if(sFullName== "")
            {
                Session["z_ReturnUrl"] = LinkHelper.Parse("/?m=Play");
                Response.Redirect("/?m=Login");
                return;
            }
        }
    }
}