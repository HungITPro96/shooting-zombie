﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Game.ascx.cs" Inherits="ShootingZombie.Module.Game.Game" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>

<style>
    div.template-wrap canvas{
        box-shadow:rgba(0, 0, 0, 0.1) 0px 6px 6px 6px;
    }
    body{
        -webkit-user-select: none;
        -moz-user-select: -moz-none;
        -ms-user-select: none;
        user-select: none;
    }
</style>

<div class="template-wrap clear text-center">
    <div class="cssload-square">
	    <div class="cssload-square-part cssload-square-green"></div>
	    <div class="cssload-square-part cssload-square-pink"></div>
	    <div class="cssload-square-blend"></div>
    </div>
    <canvas class="emscripten" style="background-color:#222222;" id="canvas" oncontextmenu="event.preventDefault()" height="600px" width="1366px">
    </canvas>
    <div class="fullscreen">
        <img src="<%=Page.ResolveClientUrl("/Content/images/fullscreen.png") %>" width="45" height="45" alt="Fullscreen" title="Fullscreen" onclick="SetFullscreen(1);" />
    </div>
</div>
    