﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace ShootingZombie.Module.Game
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string GetInformationUserItem()
        {
            int iUserId = UrlHelper.getRequestParam(Session["z_UserId"], 0);
            if (iUserId > 0)
            {
                try
                {
                    using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        List<fesp_UserItems_GetUserItemByUserId_Result> lstItems = dtc.fesp_UserItems_GetUserItemByUserId(iUserId).ToList();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        return js.Serialize(lstItems);
                    }
                }
                catch(Exception ex)
                {
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateInformationUserItem(int Experience, int Gold, List<int> Items)
        {
            int iUserId = UrlHelper.getRequestParam(Session["z_UserId"], 0);
            if (iUserId > 0)
            {
                try
                {
                    using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.fesp_Users_UpdateInformationGame(iUserId, (Experience >= 0) ? Experience : 0, (Gold >= 0) ? Gold : 0).FirstOrDefault();
                        return "1";
                    }
                }
                catch (Exception ex)
                {
                    return "-1";
                }
            }
            else
            {
                return "-2";
            }
        }
    }
}
