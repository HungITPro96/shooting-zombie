﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace ShootingZombie.Module.Login
{
    public partial class Register : System.Web.UI.UserControl
    {
        protected List<string> lstMessage;
        protected string sUserRegister, sEmail, sUser;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["z_UserId"] != null)
            {
                if ((int)Session["z_UserId"] > 0)
                {
                    Response.Redirect(LinkHelper.Parse("/"));
                }
            }

            Constants.updateTitle("Register - Shooting Zombie");

            lstMessage = new List<string>();


            sUserRegister = UrlHelper.getRequestParam(Session["z_UserRegister"], "");
            Session["z_UserRegister"] = null;

            if (Request.HttpMethod == "POST")
            {
                string sPass;
                string sButtonClick = Request.Form["btnClick"];

                #region Register
                if (sButtonClick == "btnSubmitRegister")
                {
                    sUser = Request.Form["txtUserName_Register"];
                    CryptoUtils cryto = new CryptoUtils();
                    sPass = cryto.GetMD5Hash(Request.Form["txtPassword_Register"]);
                    sEmail = Request.Form["txtEmail_Register"];

                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        int? iResult = dtc.fesp_Users_CheckExists(sUser).FirstOrDefault();
                        if(iResult == 1)
                        {
                            lstMessage.Add("Username is exists! Choose other username!");
                        }
                        else
                        {
                            ObjectParameter oUserId = new ObjectParameter("UserId", 0);
                            iResult = dtc.fesp_UserInfos_Insert(sUser, sPass, sEmail, oUserId).FirstOrDefault();
                            if ((int)oUserId.Value > 0)
                            {
                                iResult = dtc.fesp_Users_Insert((int)oUserId.Value, sUser).FirstOrDefault();
                                Session["z_UserRegister"] = sUser;
                                Response.Redirect("/?m=Login");
                            }
                        }
                    }
                }
                #endregion
            }
        }
    }
}