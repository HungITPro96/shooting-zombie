﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace ShootingZombie.Module.Login
{
    public partial class Login : System.Web.UI.UserControl
    {
        protected List<string> lstMessage;
        protected string sUserRegister;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["z_UserId"] != null)
            {
                if ((int)Session["z_UserId"] > 0)
                {
                    Response.Redirect(LinkHelper.Parse("/"));
                }
            }

            Constants.updateTitle("Login - Shooting Zombie");

            lstMessage = new List<string>();


            sUserRegister = UrlHelper.getRequestParam(Session["z_UserRegister"], "");
            Session["z_UserRegister"] = null;

            if (Request.HttpMethod == "POST")
            {
                string sUser, sPass;
                string sButtonClick = Request.Form["btnClick"];

                #region Login
                if (sButtonClick == "btnSubmitLogin")
                {
                    sUser = Request.Form["txtUserName_Login"];
                    sPass = Request.Form["txtPassword_Login"];

                    CryptoUtils cryto = new CryptoUtils();

                    string md5Pass = cryto.GetMD5Hash(sPass);
                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        fesp_Users_CheckLogin_Result iUser = dtc.fesp_Users_CheckLogin(sUser, md5Pass).FirstOrDefault();
                        if (iUser != null)
                        {
                            int? iResult = dtc.fesp_Users_UpdateLoginDate(iUser.UserId,true).FirstOrDefault();
                            Session["z_UserId"] = iUser.UserId;
                            Session["z_UserName"] = iUser.UserName;
                            Session["z_FullName"] = iUser.FullName;
                            if (Session["z_ReturnUrl"] != null)
                            {
                                Response.Redirect(Session["z_ReturnUrl"].ToString());
                                return;
                            }
                            else
                            {
                                Response.Redirect(LinkHelper.Parse("/"));
                            }
                        }
                        else
                        {
                            lstMessage.Add("Sai tên đăng nhập hoặc mật khẩu");
                        }
                    }
                }
                #endregion
                #region Register
                else if (sButtonClick == "btnSubmitRegister")
                {
                    sUser = Request.Form["txtUserName_Register"];
                    CryptoUtils cryto = new CryptoUtils();
                    sPass = cryto.GetMD5Hash(Request.Form["txtPassword_Register"]);
                    string sEmail = Request.Form["txtEmail_Register"];

                    using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                    {
                        ObjectParameter oUserId = new ObjectParameter("UserId", 0);
                        int? iResult = dtc.fesp_UserInfos_Insert(sUser, sPass, sEmail, oUserId).FirstOrDefault();
                        if ((int)oUserId.Value > 0)
                        {
                            iResult = dtc.fesp_Users_Insert((int)oUserId.Value, sUser).FirstOrDefault();
                            Session["z_UserRegister"] = sUser;
                        }
                    }
                }
                #endregion
            }
        }
    }
}