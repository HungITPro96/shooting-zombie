﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Linq;

namespace ShootingZombie.Module.Login
{
    public partial class Profile : System.Web.UI.UserControl
    {
        protected fesp_Users_GetById_Result iUser;
        protected int result = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            int sUserId = UrlHelper.getRequestParam(Session["z_UserId"], 0);

            if (sUserId == 0)
            {
                Session["z_ReturnUrl"] = LinkHelper.Parse("/?m=Profile");
                Response.Redirect("/?m=Login");
                return;
            }
            Constants.updateTitle("Profile - Shooting Zombie");
            if (Request.HttpMethod == "POST")
            {
                using (ShootingZombieEntities dtc = new ShootingZombieEntities())
                {
                    string fullName = Request.Form["txtFullName"];
                    string gender = Request.Form["txtGender"];
                    string dob = Request.Form["txtDoB"];
                    string phone = Request.Form["txtPhone"];
                    int? iResult = dtc.fesp_Users_UpdateInformation(fullName, int.Parse(gender), DateTime.Parse(dob), phone, sUserId).FirstOrDefault();
                    if (iResult == null)
                    {
                        result = -1;
                    }
                    else
                    {
                        result = 1;
                    }
                }
            }
            using (ShootingZombieEntities dtc = new ShootingZombieEntities())
            {
                iUser = dtc.fesp_Users_GetById(sUserId).FirstOrDefault();
                if (iUser == null)
                {
                    Response.Redirect("/?m=Register");
                    return;
                }

            }

            
        }
    }
}