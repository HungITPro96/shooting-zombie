﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShootingZombie.Module.Home
{
    public partial class Home : System.Web.UI.UserControl
    {
        protected List<fesp_News_ListForBanner_Result> lstNews;
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Home - Shooting Zombie");

            using (ShootingZombieEntities dtc = new ShootingZombieEntities())
            {
                lstNews = dtc.fesp_News_ListForBanner().ToList();
            }
        }
    }
}