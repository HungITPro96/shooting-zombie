﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Guide
{
    public partial class Guide : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Constants.updateTitle("Guide - Shooting Zombie");
        }
    }
}