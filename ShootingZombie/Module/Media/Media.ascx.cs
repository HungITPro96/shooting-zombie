﻿using ShootingZombie.Content.Logic;
using System;

namespace ShootingZombie.Module.Media
{
    public partial class Media : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Constants.updateTitle("Media - Shooting Zombie");
        }
    }
}