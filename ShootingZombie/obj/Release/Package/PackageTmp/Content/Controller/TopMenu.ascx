﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenu.ascx.cs" Inherits="ShootingZombie.Content.Controller.TopMenu" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>

<% if(sModule != "Login" && sModule != "Register" && sModule != "Play") { %>
    <div class="header">
		    <div class="w3layouts_header_left">
			    <div class="top-nav-text">
				    <p>Support Online : <span class="call"><a href="tel:0988 888 999">0988 888 999</a></span></p>
				    <p>Email Us : <span class="mail"><a href="mailto:support@shootingzombie.com">support@shootingzombie.com</a></span></p>
			    </div>
		    </div>
		    <div class="w3layouts_header_right text-right">
                <% if (sFullName == "") { %>
			        <a href="/?m=Register"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Register</a>
                    &nbsp;
			        <a href="/?m=Login"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Login</a>
                <%} else { %>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Welcome <%=sFullName %>
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<%= LinkHelper.Parse("/?m=Profile") %>"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Profile</a></li>
                                <li><a href="<%= LinkHelper.Parse("/?m=Logout") %>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                <%} %>
		    </div>
		    <div class="clearfix"> </div>
    </div>
<%} %>
<div class="w3_navigation">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="w3_navigation_pos">
						<h1><a href="<%=LinkHelper.Parse("/") %>"><span>Shooting Zombie</span></a></h1>
					</div>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="cl-effect-5" id="cl-effect-5">
						<ul class="nav navbar-nav">
                            <li class="<%=ActiveCurrentModule("Home") %>">
                                <a href="<%=LinkHelper.Parse("/") %>" title="Home">
                                    Home
                                </a>
                            </li>
                            <li class="<%=ActiveCurrentModule("News") %>">
                                <a href="<%=LinkHelper.Parse("/?m=News") %>" title="Infomation about game and event" class="scroll">
                                    News
                                </a>
                            </li>
                            <li class="<%=ActiveCurrentModule("Webshop") %>">
                                <a href="<%=LinkHelper.Parse("/?m=Webshop") %>" title="Buy & Trade items" class="scroll">
                                    Webshop
                                </a>
                            </li>
                            <li class="<%=ActiveCurrentModule("Casino") %>">
                                <a href="<%=LinkHelper.Parse("/?m=Trade") %>" title="Casino" class="scroll">
                                    Trade
                                </a>
                            </li>
                            <li class="<%=ActiveCurrentModule("Guide") %>">
                                <a href="<%=LinkHelper.Parse("/?m=Guide") %>" title="Guide" class="scroll">
                                    Guide
                                </a>
                            </li>
                            <li class="<%=ActiveCurrentModule("Media") %>">
                                <a href="<%=LinkHelper.Parse("/?m=Media") %>" title="Video game" class="scroll">
                                    Media
                                </a>
                            </li>
							<%--<li class="active"><a href="index.html"><span data-hover="Home">Home</span></a></li>

							<li><a href="#about" class="scroll"><span data-hover="About">About</span></a></li>
							<li><a href="#services" class="scroll"><span data-hover="Services">Services</span></a></li>
							<li><a href="#work" class="scroll"><span data-hover="Gallery">Gallery</span></a></li>
							<li><a href="#projects" class="scroll"><span data-hover="News">News</span></a></li>
							<li><a href="#mail" class="scroll"><span data-hover="Contact">Contact</span></a></li>--%>
						</ul>
					</nav>
				</div>
			</nav>	
		</div>
	</div>

<%--<div class="header clearfix">
    <nav class="navbar navbar-inverse">
        <div class="container text-center">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<%=LinkHelper.Parse("/?m=News") %>" title="Thông tin về sự kiện và game">Tin tức</a>
                </li>
                <li>
                    <a href="<%=LinkHelper.Parse("/?m=Webshop") %>" title="Mua vật phẩm game">Webshop</a>
                </li>
                <li class="logo-title">
                    <a href="<%=LinkHelper.Parse("/?m=Home") %>" title="Trang chủ">
                        Trang chủ
                    </a>
                </li>
                <li>
                    <a href="<%=LinkHelper.Parse("/?m=Guide") %>" title="Hướng dẫn chơi game">Hướng dẫn</a>
                </li>
                <li>
                    <a href="<%=LinkHelper.Parse("/?m=Livestream") %>" title="Xem trực tiếp thi đấu">Livestream</a>
                </li>
            </ul>
        </div>
    </nav>
</div>--%>