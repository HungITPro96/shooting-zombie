﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterMenu.ascx.cs" Inherits="ShootingZombie.Content.Controller.FooterMenu" %>
<div class="footer_container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 footer_leftside">
                <h3>Shooting Zombie</h3>
                <p>Đàm Thanh Tùng - Nguyễn Xuân Hùng</p>
                <p class="copyright">COPYRIGHT © Shooting Zombie Team 2017</p>
            </div>
            <div class="col-md-4 footer_middleside">
                <table>
                    <tbody>
                        <tr>
                            <td style="font-weight:700; font-size:13px;">CONTACT US</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="tel:0988 888 999"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;0988 888 999</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="mailto:support@shootingzombie.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;support@shootingzombie.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 footer_rightside">
                <ul>
                    <li>
                        <a href="/?m=Guide"><i class="fa fa-gamepad" aria-hidden="true"></i>&nbsp;Guide</a>
                    </li>
                    <li>
                        <a href="/?m=News"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;News</a>
                    </li>
                    <li>
                        <a href="/?m=Webshop"><i class="fa fa-gift" aria-hidden="true"></i>&nbsp;Webshop</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i>&nbsp;FAQ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>