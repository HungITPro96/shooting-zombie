﻿jQuery(document).ready(function ($) {
        // Slideshow 4
    $("#slider3").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

    $(".scroll").click(function (event) {
        $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
    });

    $('#score_period').kendoDropDownList();

    

});