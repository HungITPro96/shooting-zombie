﻿$(document).ready(function () {
    $('.profile-sideBar a').click(function (e) {
        e.preventDefault();
        $('.profile-rightContent').hide();
        $('.profile-sideBar li').each(function () {
            $(this).removeClass('active');
        });
        $(this).closest('li').addClass('active');
        var id = $(this).attr('ref');
        $('#' + id).fadeIn(350);
    });

    var curDate = new Date();
    $('#txtDoB').kendoDatePicker({
        max: new Date(curDate.getFullYear() - 10, 0, 1),
        start: "decade",
        format:"dd/MM/yyyy"
    });

    $('#txtGender').kendoDropDownList({
        dataSource: [
              { text: "Gender", value: 0 },
              { text: "Male", value: 1 },
              { text: "Female", value: 2 },
              { text: "Other", value: 3 }
        ],
        dataTextField: "text",
        dataValueField: "value"
    });

    if (typeof result != "undefined") {
        if ($('body').find('showResult').length == 0) {
            $('body').append('<div id="showResult" class="modal fade" role="dialog">' +
                              '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                  '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                                    '<h4 class="modal-title"><i class="fa fa-check-circle-o" style="color: #4CAF50;" aria-hidden="true"></i>&nbsp;Update Successfully</h4>' +
                                  '</div>' +
                                '</div>' +
                              '</div>' +
                            '</div>'
                            );
        }
        if (result == 1)
            $('#showResult').modal('show');

    }
})