﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content/MasterPage/Simple.Master" AutoEventWireup="true" CodeBehind="500.aspx.cs" Inherits="ShootingZombie.Module.Error._500" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SimplePage_Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SimplePage_Top" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="mainContent error-content clearfix">
        <div class="container text-center">
        <div class="error-content-top">
            <h1>500</h1>
            <h3>Error</h3>
        </div>
        <h3>Ôi trời</h3>
        <p>Có lỗi xảy ra rồi. Xin vui lòng</p>
        <p><a href="#" onclick="window.history.back();">Quay lại</a> ? hoặc <a href="#">Liên hệ</a> với chúng tôi</p>
        </div>
    </div>        
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SimplePage_Footer" runat="server">
    <div style="display:none;"><%=sError %></div>
</asp:Content>
