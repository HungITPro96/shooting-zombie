﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home.ascx.cs" Inherits="ShootingZombie.Module.Home.Home" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>
<%@ Import Namespace="ShootingZombie.Content.Data" %>

<div class="banner">
    <div class="slider">
        <div class="callbacks_container">
            <ul class="rslides" id="slider3">
                <% int i = 1;
                    foreach (fesp_News_ListForBanner_Result iNews in lstNews)
                    { string sSrc = "~/Content/images/banner/banner"+i+".jpg";%>
                    <li>
                        <div class="slider-img">
						    <img src="<%=Page.ResolveClientUrl(sSrc) %>" class="img-responsive" alt="Shooting Zombie"/>
                        </div>
                        <div class="slider-info">
                            <h4><%=iNews.Title %></h4>
                            <p><%=iNews.Headline %></p>
						    <a href="<%=LinkHelper.Parse("/?m=News&c="+iNews.GroupAlias+"&i="+iNews.Alias+"-"+iNews.NewsId) %>" class="hvr-shutter-in-horizontal scroll">Read More</a>
                        </div>
                    </li>
                    <% i++; if (i == 4) i = 1; } %>
			</ul>
        </div>
		<div class="clearfix"></div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3 side_bar">
            <div class="play_now">
                <a href="<%=LinkHelper.Parse("/?m=Play") %>" title="Play now" class="play_link">
                    <img src="<%=Page.ResolveClientUrl("/Content/images/play_now.png") %>" width="100%"/>
                </a>
            </div>
            <div class="top_score panel panel-default" style="display:none;">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 top_score_title">
                            <h3>TOP SCORE</h3>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <select id="score_period" style="width:100%;">
                            <option value="1">In Week</option>
                            <option value="2">In Month</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <h2>Content</h2>
        </div>
    </div>
</div>
