﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ShootingZombie.Module.Login.Login" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>
<div class="row login-content">
     <div class="container">
        <form role="form" method="post" class="form-signin" id="frmLogin">
            <input type="hidden" id="btnClick" name="btnClick" value="" />
            <div id="frmLoginForm">
                <h2 class="form-signin-heading animated slideInUp">Login</h2>
                <div class="input-group animated slideInUp">
                    <input type="text" class="text" required="required" autocomplete="true" id="txtUserName_Login" name="txtUserName_Login" value="<%=sUserRegister %>" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="forText">User name</label>
                    <div style="height: 25px;">
                        <label id="txtUserName_Login-error" class="error" for="txtUserName_Login"></label>
                    </div>
                </div>

                <div class="input-group animated slideInUp">
                    <input type="password" class="text" required="required" autocomplete="off" id="txtPassword_Login" name="txtPassword_Login"/>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="forText">Password</label>
                    <div style="height: 25px;">
                        <label id="txtPassword_Login-error" class="error" for="txtPassword_Login"></label>
                    </div>
                </div>

                <div class="input-group animated slideInUp">
                    <a href="/?m=Register" title="Register" id="btnRegister" class="btn btn-link">Not have account yet? Create a new one</a>
                </div>
                <%if (lstMessage.Count > 0) { %>
                <div class="input-group error animated slideInUp">
                    <%foreach (string item in lstMessage) {%>
                            <span>
                                <%=item %>
                            </span>
                       <% } %>
                </div>
                <%} %>
                <%--<div class="input-group">
                    <input type="checkbox" id="chbRememberMe" />
                    <label for="chbRememberMe">Remember me</label>
                </div>--%>
                <button type="submit" class="btn btn-lg btn-primary btn-submit animated slideInUp" id="btnSubmitLogin"  data-loading-text="<i class='glyphicon glyphicon-refresh spinning'></i>&nbsp;Signing in...">Sign in</button>
                <div class="input-group animated slideInUp text-center">
                    <div style="color:#333;">OR</div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <button class="loginBtn loginBtn--facebook" type="button">
                                Facebook
                            </button>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <button class="loginBtn loginBtn--google" type="button">
                                Google
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/Content/js/jquery.validate.min.js"></script>
