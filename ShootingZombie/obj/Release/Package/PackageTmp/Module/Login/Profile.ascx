﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="ShootingZombie.Module.Login.Profile" %>
<div class="container profile">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <h2><i class="fa fa-user" aria-hidden="true"></i> My profile</h2>
        </div>
        <div class="col-md-12 col-lg-12 col-xs-12 profile-top">
            <div class="col-md-4 col-xs-12 profile-name">
                <div class="profile-avatar">
                    <img src="../../Content/images/profile-default.svg" width="100%" />
                </div>
                <div class="profile-username">
                    <span>
                        <%=iUser.UserName %>
                    </span>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 experience">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Experience
                        </div>
                    </div>
                    <div class="panel-body animated bounceIn">
                        <span>1000 xp</span>
                        <span>(Lv 5)</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 gold">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Wallet
                        </div>
                    </div>
                    <div class="panel-body animated bounceIn">
                        <span>2000 G</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12 profile-content">
            <div class="col-md-3 col-xs-12 profile-sideBar">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active">
                        <a href="#" title="Information" ref="information">
                            <i class="fa fa-user-secret fa-3x" aria-hidden="true"></i>
                            <div class="sideBar-title">General Information</div>
                            <div class="sideBar-dec">View and update profile</div>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="History" ref="history">
                            <i class="fa fa-history fa-3x" aria-hidden="true"></i>
                            <div class="sideBar-title">History transaction</div>
                            <div class="sideBar-dec">History buy and trade items</div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 col-xs-12 profile-Content">
                <form method="post" role="form">
                    <div id="information" class="profile-rightContent">
                        <h3>Information</h3>
                        <div class="information-detail">
                            <div class="form-horizontal" >
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="txtFullName">FullName:</label>
                                    <div class="col-sm-10"> 
                                        <input type="text" class="form-control" name="txtFullName" id="txtFullName" placeholder="FullName" value="<%=iUser.FullName %>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="txtEmail">Email:</label>
                                    <div class="col-sm-10"> 
                                        <input type="email" class="form-control" id="txtEmail" placeholder="Email" disabled value="<%=iUser.Email %>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="txtGender">Gender:</label>
                                    <div class="col-sm-10"> 
                                        <input type="text" id="txtGender" name="txtGender" value="<%=iUser.Gender %>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="txtDoB">Date of Birth:</label>
                                    <div class="col-sm-10"> 
                                        <input type="text" id="txtDoB" name="txtDoB" value="<%=(iUser.DateOfBirth !=null)?iUser.DateOfBirth.GetValueOrDefault().ToString("dd/MM/yyyy"):new DateTime(2010,1,1).ToString("dd/MM/yyyy") %>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="txtPhone">Phone:</label>
                                    <div class="col-sm-10"> 
                                        <input type="text" class="form-control" name="txtPhone"  id="txtPhone" placeholder="Enter your phone" value="<%=iUser.PhoneNumber %>">
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="history" style="display:none" class="profile-rightContent">
                        <h3>History</h3>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var result = '<%=result %>'
</script>
