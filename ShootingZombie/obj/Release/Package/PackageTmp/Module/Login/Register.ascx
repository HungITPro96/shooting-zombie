﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Register.ascx.cs" Inherits="ShootingZombie.Module.Login.Register" %>
<%@ Import Namespace="ShootingZombie.Content.Logic" %>

<div class="row login-content">
     <div class="container">
        <form role="form" method="post" class="form-register" id="frmLogin">
            <input type="hidden" id="btnClick" name="btnClick" value="" />
            <div id="frmRegisterForm">
                <h2 class="form-signin-heading">Register</h2>
                <div class="input-group animated slideInUp">
                    <input type="text" class="text" required="required" value="<%=sUser %>" autocomplete="true" id="txtUserName_Register" name="txtUserName_Register" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="forText">User name</label>
                    <div style="height: 25px;">
                        <label id="txtUserName_Register-error" class="error" for="txtUserName_Register"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                         <div class="input-group animated slideInUp">
                            <input type="password" class="text" required="required" autocomplete="off" id="txtPassword_Register" name="txtPassword_Register"/>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="forText">Password</label>
                            <div style="height: 25px;">
                                <label id="txtPassword_Register-error" class="error" for="txtPassword_Register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="input-group animated slideInUp">
                            <input type="password" class="text" required="required" autocomplete="off" id="txtPasswordRef_Register" name="txtPasswordRef_Register"/>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label class="forText">Reenter password</label>
                            <div style="height: 25px;">
                                <label id="txtPasswordRef_Register-error" class="error" for="txtPasswordRef_Register"></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="input-group animated slideInUp">
                    <input type="text" class="text" required="required" value="<%=sEmail %>" autocomplete="off" id="txtEmail_Register" name="txtEmail_Register"/>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <label class="forText">Email</label>
                    <div style="height: 25px;">
                        <label id="txtEmail_Register-error" class="error" for="txtEmail_Register"></label>
                    </div>
                </div>

                <div class="input-group animated slideInUp">
                    <a href="/?m=Login" title="Login" id="btnLogin" class="btn btn-link">Have an account? Login now</a>
                </div>
                <%if (lstMessage.Count > 0) { %>
                <div class="input-group error  animated slideInUp">
                    <%foreach (string item in lstMessage) {%>
                            <span>
                                <%=item %>
                            </span>
                       <% } %>
                </div>
                <%} %>
                <%--<div class="input-group">
                    <input type="checkbox" id="chbRememberMe" />
                    <label for="chbRememberMe">Remember me</label>
                </div>--%>
                <button type="submit" class="btn btn-lg btn-primary btn-submit animated slideInUp" id="btnSubmitRegister" data-loading-text="<i class='glyphicon glyphicon-refresh spinning'></i>&nbsp;Register...">Register</button>
                <div class="input-group animated slideInUp text-center">
                    <div style="color:#333;">OR</div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <button class="loginBtn loginBtn--facebook" type="button">
                                Facebook
                            </button>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <button class="loginBtn loginBtn--google" type="button">
                                Google
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="/Content/js/jquery.validate.min.js"></script>