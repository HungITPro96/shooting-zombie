﻿using ShootingZombie.Content.Data;
using ShootingZombie.Content.Logic;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Linq;

namespace ShootingZombie
{
    public partial class Default : Page
    {
        protected string sModule = "Home", sCategory, sItem;
        protected bool existJsFile = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            sModule = UrlHelper.getRequestParam(Request.Params["m"],"Home");

            if(sModule == "Guide")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Guide/Guide.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }else if(sModule == "News")
            {
                sCategory = UrlHelper.getRequestParam(Request.Params["c"], "");
                sItem = UrlHelper.getRequestParam(Request.Params["i"], "");
                UserControl ctrl;
                if (sCategory != "")
                {
                    if(sItem != "")
                    {
                        ctrl = (UserControl)Page.LoadControl("/Module/News/Detail.ascx");
                    }else
                    {
                        ctrl = (UserControl)Page.LoadControl("/Module/News/Category.ascx");

                    }
                }else
                {
                    ctrl = (UserControl)Page.LoadControl("/Module/News/News.ascx");
                }
                contentPlaceHolder.Controls.Add(ctrl);
            }else if(sModule == "Home")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Home/Home.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }else if(sModule == "Play")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Game/Game.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }else if(sModule == "Media")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Media/Media.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }else if (sModule == "Webshop")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Webshop/Webshop.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            } else if(sModule == "Login")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Login/Login.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            } else if(sModule == "Register")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Login/Register.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }
            else if (sModule == "Profile")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Login/Profile.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }
            else if (sModule == "Logout")
            {
                using(ShootingZombieEntities dtc = new ShootingZombieEntities())
                {
                    int? iResult = dtc.fesp_Users_UpdateLoginDate((int)Session["z_UserId"], false).FirstOrDefault();
                }
                Session.Clear();
                Response.Redirect("/");
            }
            else if (sModule == "Trade")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Trade/Trade.ascx");
                contentPlaceHolder.Controls.Add(ctrl);
            }
            else if (sModule == "404")
            {
                UserControl ctrl = (UserControl)Page.LoadControl("/Module/Error/404.aspx");
                contentPlaceHolder.Controls.Add(ctrl);
            }

            if (File.Exists(HttpContext.Current.Server.MapPath("/Content/js/js_" + sModule + ".js")))
                existJsFile = true;
        }
    }
}